﻿plots
=====

.. automodule:: plots

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      gwh_to_twh
      plot_heat_demand
      plot_params
      plot_population_all
      plot_scenarios
      save_to
      set_title
   
   

   
   
   

   
   
   



