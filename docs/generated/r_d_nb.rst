﻿r\_d\_nb
========

.. automodule:: r_d_nb

   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      apply_r_d
      calc_r_d_final
      calc_r_d_ic
      demolition
      merge_r_d_final
      new_buildings
      remaining_r_d
      restauration
   
   

   
   
   

   
   
   



