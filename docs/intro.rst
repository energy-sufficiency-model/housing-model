.. housing-model documentation master file, created by
   sphinx-quickstart on Wed Apr 12 13:55:55 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Description of the Model
========================


The model presented here is a bottom-up building stock model. 
We have used data from the IWU Institute/Tabula, which allows us to consider 40 different buildings, each with three levels of refurbishment (no refurbishment, standard refurbishment and ambitious refurbishment). The aim is to model the development of the building stock in Germany over 40 years (with an annual time step). Influencing factors are the rate and degree of refurbishment, new construction and demolition, as well as population growth, living space per person and reductions in hot water and space heating demand. 

Scenarios are computed with a techno-economic bot­tom-up simulation model for the residential heating sector in Germany. The developed model covers the annual development of the building stock (demand side) from 2020 to 2060. Figure 1 provides an overview of the model. The starting point of the simulation is the year 2019, which repre­sents the the status-quo of the average living space of the residential sector and the related heat demand. The future development of the total living space depends on the population and the average living space per person. Both factors are exogenous model variables. The related heat demand depends on hot-water-demand, building condition, outdoor temperature and related heating behaviour. The building- condition are influenced by restoration rates and depth of restoration which are also exogenous model variables. The outside temperature and heating behaviour cannot be changed in the model and are fixed with adjusted data from Loga et al. (2015). The development of the building-stock (endogenous) within the period under consideration is influenced by these mentioned factors. 


Building stock
--------------

The annual heat demand is derived from a detailed stock model of buildings and their respective energy demand. Below, we outline the general structure of the model and introduce the relevant input data. 
The structure of the buildings classes as well as their data are based on Loga et al. (2015). One of the central units is the energy demand for heat and hot water. 

Building classes 
----------------

In the model, all residential homes are clustered into building-classes based on a detailed analysis of the current building stock in Germany conducted by Loga et al. (2015). Two characteristic values are consid­ ered: 1) the year of construction and 2) the building type (e.g. single-family or apartment houses) and three states of the energetic status. In total, this results in 120 different building-classes. These building classes are not affected by an increase or decrease in average living space but the number of buildings can change. 

Definition of living space 
--------------------------

The living space refers to the living area in buildings and does not cover further usable areas. Moreover, only buildings with more living space than effective areas are counted. Other living arrangements like dormitories are included and counted as apartment buildings. 

Energetic restoration
---------------------
 
Following the structure of Loga et al. (2015), we distinguish between three categories regarding the restoration state: (1) unrenovated, (2) standard, and (3) the ambitious restoration. The standard restoration corresponds to current energy saving regulations (german:Energieeinsparverordnung (EnEV), and since 2020 Gebäudeenergiengesetz (GEG)) and results in a reduced heat demand in the range of 59-129 kWh/m 2 depending on the type of house.The ambitious restoration corresponds to the so-called passive house standard and results in a reduced heat demand of 14-57 kWh/m 2 . The specific measures and their effects are based on the detailed analysis of Loga et al. (2015, p.26-31). 

Monument protection
-------------------

Monument protection determines which buildings cannot be restored due to various reasons, such as their protected status or it can also be used to model the effects of buildings that have not been fully restored in terms of energy efficiency. These buildings are excluded from restoration and demolition. 

Distribution of restoration and demolition
------------------------------------------

The distribution of restoration and demolition is not directly based on the age and lifespan of buildings in the model. Instead, it is possible to define a preferred distribution of restoration and demotlition/vacany rates. Given that older buildings are typically in worse condition the distribution can be pre-defined to prioritise energetic renovation for older buildings. The same approach can be applied to demolition/vacancy. The model uses this as a starting point for the distribution of demolition/vacancy and renovation. However, if certain building classes are already fully restored (restricted by monument protection), the model calculates a new distribution of renovation and demolition/vacancy, taking into account the predefined distribution of the remaining building classes. This can lead to the effect that younger buildings are restored later, while older buildings are restored earlier.

