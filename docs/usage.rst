Usage
=====

.. _installation:

Installation
------------

Ensure that you have *python >= 3.9.13* installed. To see your python version, open your terminal and type:

.. code-block:: console

  $ python --version
  
- navigate with your terminal to the cloned folder. E.g. 

.. code-block:: console

  $ cd ~/housing-model/
  
- choose one of the following possibilities to install the needed requirements
    - To install the needed librarys, follow any of the following options:
      
      - pip:
      
        .. code-block:: console
      
          $ pip3 install -r requirements.txt
    
      - anaconda:
      
        .. code-block:: console
      
          $ conda install --file requirements.txt
      
      - apt:
      
        .. code-block:: console
      
          $ sudo apt install < requirements.txt

- it might occur that you are getting warnings with missing librarys/flags for pyplotlib. Ensure that you have the mesa-utils package installed

Inputs
------
In the "input" folder you will find files for setting the hyperparameters and defining the scenarios.    

The hyperparemeter contain parameters that apply to all scenarios.  

- The `hyperparameter.xlsx` file contains: 

  - Population developement (bev_variant)
  
  - Scenarios to be calculated (scenario)
  
  - Considering building types (SFH, MFH..) for renovation (restauration_building_type bias) (the yes-case is not yet implemented)
  
  - Decide whether a second energetic modernisation should be possible (second_amb_restauration) 
  
  - Define where the output should be saved (output_folder) 
  
The scenario file contains all the parameters that define the scenarios. You can either define your scenarios in the `scenario_parameter.xlsx` or open it with your office application and find the different scenarios in the spreadsheets.

- The scenario parameters can be set for the years 2020, 2030, 2040, 2050 and 2060 and are the following

  - annual renovation rate (restoration_rate)
   
  - renovation depth (restoration_deep_amb) 
   
  - distribution between building_types for restoration of SFH, TH, MFH and AB (e.g. restoration_sfh)
   
  - lower limit for demolition (demolition_rate_min)
   
  - distribution between building_types for demolition of SFH, TH, MFH and AB (e.g. demolition_sfh)
   
  - lower limit for new construction (new_building_rate_min)
   
  - energetic status of new buildings (new_building_deep_amb)
   
  - distribution between building_types for new construction of SFH, TH, MFH and AB (e.g. new_building_share_sfh)
   
  - living space per capita (living_space_pc)
   

.. - The `scenario_parameter.xlsx` file contains all scenarios.
 .. - to access and change it, either:
..   - navigate inside your terminal inside the input-folder (`cd input`)
..    - then run the `manage_params.py` file by typing `python3 manage_params.py` into the terminal. A new window will appear
..   - follow the instructions of the new window
    

Data
----

The data that is used for this project can be found inside the data-folder. These contain the demographic developement, the distribution assumptions for demolition, renovation and monoment protection, the building stock for 2019 and the building typology. 

- `12421-0001.xlsx <https://www-genesis.destatis.de/genesis/online?operation=table&code=12421-0001&bypass=true&levelindex=0&levelid=1643115777925#abreadcrumb>`_: Demographic developement of germany based on different population models

- `distribution_buildings.xlsx`: Distribution of demolition, renovation rate and monoment protection over building types

- `share_buildings_2019.xlsx`: Building-stock for 2019 with Tabula-ID and living space in each building-class in million square meteres for 2019. The buildingstock for 2019 has been modelled based on a `weighted building typologiy from 2011 <https://www.iwu.de/fileadmin/user_upload/dateien/energie/klima_altbau/Fl%C3%A4chen_Geb%C3%A4udetypologie_Okt_2013.pdf>`_ and supplemented by `energetic modernisation <https://www.iwu.de/fileadmin/publikationen/gebaeudebestand/2018_IWU_CischinskyEtDiefenbach_Datenerhebung-Wohngeb%C3%A4udebestand-2016.pdf>`_ and new buildings and demolition

- `TABULA-Analyses_DE-Typology_ResultData.xlsx <https://www.iwu.de/fileadmin/tools/tabula/TABULA-Analyses_DE-Typology_DataTables.zip>`_: 

  - Building typology data from IWU (Institut Wohnen und Umwelt) which contain all building classes used according to:

   - construction year
  
   - building type (SFH, TH, MFH and AB)
  
   - state of renovation

  - as well as data which can be used for the calculation of heat demand and soil sealing:
 
   - living space (m2)
  
   - specific heat demand (kWh/m2)
  
   - specific hot water demand (kWh/m2)
  
   - roof area (m2)
  
   - floor area (m2)
  
It is also possible to select different data sets. We have selected the data for Germany that contain the heat and hot water behaviour of residents of different buildings.   

Outputs
-------

In the output folder all generated output figures, as well as tables are saved.
The outpus are devided into: 

.. - `population comparison`: the possible population development assumptions. For getting an idea which population development to choose for own models.

- `params comparisons`: figures of all scenario parameters

- `scenario comparison`: if more than one scenario is computed by the model, you fill find figures that compare scenario outputs that cover `living space per capita, total living space, specific average heat demand and total heat demand`.

- `scenario_name`: scenario specific tables and figures which contain:  

  - `results.xlsx`: this file deliver all results for each year(2019-2060) in six spreadsheets. The spreadsheets contain
  
    - `building_stock`: the developement of the building stock (of each building-class) covering `restoration area, calculated living space, demolition area, new_building area, resulting living space, space heat demand` and `hot water demand` for each year.
    
    - `heat_demand`: the developement of the heat demand (of each building-class) covering space `heat demand` and `hot water demand`

    - `soil_sealing`: the developement of the soil sealing (of each building-class)
    
    - `nb_sealing_stock`: the developement of the soil sealing only by new buildings
    
    - `total_living_space`: the developement of the living space (of each building-class) 

  - `heat demand comparison.png`: heat demand divided acoording to low (<90 kWh/m2), middle (>= 90 kWh/m2 and < 120 kWh/m2) and and high (>= 120 kWh/m2) specific heat demand
  
  - `scenario_name: Heat need of building+aging classes.png`: Heat demand diveded according to years of construction (A to L) 
  
  - `scenario_name:Space heat demand comparison.png`: Comparison of the relative change of the specific heat demand and total heat demand
  
  - `scenario_name: Total heat demand comparison.pgn`: Comparison of the developement of the specific heat demand (kWh/m2) and total heat demand (TWh)

Running the model
-----------------

- Run the model by opening the terminal, navigating to the project folder and executing:
  
  .. code-block:: console
  
    $ python3 model.py

- for getting debug messages at the terminal and year-wise data files inside the scenario outputs folders, run
    
  .. code-block:: console
  
    $ python3 model.py --debug

