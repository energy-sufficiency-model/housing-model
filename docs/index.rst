.. housing-model documentation master file, created by
   sphinx-quickstart on Wed Apr 12 13:55:55 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the documentation of the building-stock-model Inhabit-Stock!
=======================================================================

Contents
--------

.. toctree::

   intro
   usage
   code
   api


Check out the :doc:`usage` section for further information, including how to
:ref:`install <installation>` the project.

