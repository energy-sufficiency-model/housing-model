API
===

.. autosummary::
  :toctree: generated

  model
  r_d_nb
  misc
  plots
  inputs
