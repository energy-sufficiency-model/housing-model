Understanding the code
======================

Code from model.py
------------------
.. automodule:: model
  :members:
  :undoc-members:  
  :noindex:


Code from /scripts/r_d_nb.py
----------------------------
.. automodule:: scripts.r_d_nb
  :members:
  :undoc-members:
  :noindex:

Code from /scripts/inputs.py
----------------------------

.. automodule:: scripts.inputs
  :members:
  :undoc-members:
  :noindex:

Code from /scripts/misc.py
--------------------------

.. automodule:: scripts.misc
  :members:
  :undoc-members:
  :noindex:

Code from /scripts/plots.py
---------------------------

.. automodule:: scripts.plots
  :members:
  :undoc-members:
  :noindex:
