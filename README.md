# Installation
- Make sure that you have `python >= 3.9.13` installed. To see your python version, open your terminal and type: `python --version`
- navigate with your terminal to the cloned folder. E.g. `cd ~/housing-model/`
- choose one of the following possibilities to install the needed requirements
    - To install the needed librarys, either type: `pip3 install -r requirements.txt`
    - or, if you prefer anaconda, type: `conda install --file requirements.txt` in your terminal. 
    - Or, as third possibility, you can install the given librarys from requirements.txt directly from your packagemanager. With ubuntu, this should do the trick:`sudo apt install < requirements.txt`
- it might occur that you are getting warnings with missing librarys/flags for pyplotlib. Ensure that you have the mesa-utils package installed

# Inputs
The inputs can be finetuned and changed. You will find all inputs inside the input-folder.
- The `hyperparameter.xlsx` file contains: 
    - population developement variant (bev-variant)
    - scenario (choose either one single scenario, or comma-separated, or type `all`)
    - restauration_building_type bias (the yes-case is not yet implemented)
    - second_amb_restauration (the yes-case is not yet implemented)
    - output_folder (the folder relative to your housing-model path where you want the output to be saved)
- The `scenario_parameter.xlsx` file contains all scenarios.
    - to access and change it, navigate inside your terminal inside the input-folder (`cd input`)
    - then run the `manage_params.py`file by typing `python3 manage_params.py`into the terminal. A new window will appear
    - follow the instructions of the new window

# Data
The data that is used for this project can be found inside the data-folder.
- `12421-0001.xlsx`: Demographic developement of germany based on different population models [^1]
- `distribution_buildings.xlsx`: Distribution of demolition and restauration rate over building types
- `share_buildings_2019.xlsx`: Tabula-ID and calculated living space in million square meteres for 2019.  Weighting of the building typology in 2019 based on a weighted building
typology from 2011 [^2] and supplemented by energetic modernisation [^3] and new buildings and demolition
- `TABULA-Analyses_DE-Typology_ResultData.xlsx`: Main data ressource for calculating the model outputs. Building typology data from IWU (Institut Wohnen und Umwelt)[^4]

# Outputs
In the output folder all generated output figures, as well as .xlsx tables are saved to disk.
- `population comparison`: the possible population development assumptions in two figures. For getting an idea which population development to choose for own models.
- `params comparisons`: figures of parameters of all models that are used by the model configuration.
- `scenario comparison`: if more than one scenario is computed by the model, figures that compare scenario outputs are saved here.
- `soe, eff, sme, ...`: scenario figures and:
- - `heat_demand_dev.xlsx`: for each year (2019-2060) the heat demand information is saved to be used by the different plots.
- - `building_stock_dev.xlsx`: living space, restoration area, demolition area, new_building area etc. for each year in the table.

# Running the model
- Run the model by opening the terminal, navigating to the project folder and executing:
    - `python3 model.py`
    - `python3 model.py --debug` for getting debug messages at the terminal and year-wise data files inside the scenario outputs folders





[^1]: [German Demographic Developement](https://www-genesis.destatis.de/genesis/online?operation=table&code=12421-0001&bypass=true&levelindex=0&levelid=1643115777925#abreadcrumb)
[^2]: [Weighted building typologiy from 2011](https://www.iwu.de/fileadmin/user_upload/dateien/energie/klima_altbau/Fl%C3%A4chen_Geb%C3%A4udetypologie_Okt_2013.pdf)
[^3]:[Energetic Modernisation](https://www.iwu.de/fileadmin/publikationen/gebaeudebestand/2018_IWU_CischinskyEtDiefenbach_Datenerhebung-Wohngeb%C3%A4udebestand-2016.pdf)
[^4]: [Tabula data table](https://www.iwu.de/fileadmin/tools/tabula/TABULA-Analyses_DE-Typology_DataTables.zip)
