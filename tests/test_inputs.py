"""

@author: guuug
"""

import unittest
import pandas as pd
import misc_tests
import scripts.inputs as inputs
import scripts.misc as misc
import json
# this adds the root directory to our path to be able to load data from root

DL = inputs.DataLoader()
IL = inputs.InputLoader()


class Test_DataLoader(unittest.TestCase):

    # assure that the tabula table is loaded and still in it's path
    def test_load_tabula(self):
        # assert that the file that is being loaded exists
        misc_tests.test_path_exists(self, misc.TABULA)
        df = DL.load_tabula(misc.TABULA)

        # test if file is empty
        self.assertIsNotNone(df)

        # test if all building types are in the dataframe
        building_types = [
            'EFH',
            'RH',
            'MFH',
            'GMH',
            'Sub-Typen',
        ]
        check_column_list = df['building_type'].to_list()
        misc_tests.check_in(self, building_types, check_column_list)

    # assure that the demographic dev table is loaded and still in it's path
    def test_load_demographic_developement(self):
        misc_tests.test_path_exists(self, misc.DEMOGRAPHIC_DEVELOPEMENT)
        # first: check file
        columns = 'A, D:AR'
        rows_start = 5
        rows_end = 35
        df = pd.read_excel(misc.DEMOGRAPHIC_DEVELOPEMENT,
                           usecols=columns,
                           skiprows=rows_start,
                           nrows=rows_end - rows_start)
        df.rename(columns={'Unnamed: 0': 'bev_variants'}, inplace=True)

        # test if file is empty
        self.assertIsNotNone(df)

        # test if all building types are in the dataframe
        # zfill does zero padding
        bev_variants = [
            'BEV-VARIANTE-{}'.format(str(i + 1).zfill(2)) for i in range(21)
        ]
        bev_models = [
            'BEV-MODELL-{}'.format(str(i + 1).zfill(2)) for i in range(9)
        ]
        bev_all = bev_variants + bev_models
        year_start = 2020
        year_end = 2060
        year_range = year_end - year_start + 1
        check_days = [
            '31.12.{}'.format(i + year_start) for i in range(year_range)
        ]
        variants = df['bev_variants'].to_list()
        headers = df.keys().to_list()

        misc_tests.check_in(self, bev_all, variants)
        misc_tests.check_in(self, check_days, headers)

        # second: check funktion
        dem_dev = DL.load_demographic_developement(
            misc.DEMOGRAPHIC_DEVELOPEMENT)
        misc_tests.check_in(self, bev_all, dem_dev.keys())

    # assure that the share_buildings table is loaded and still in it's path
    def test_load_share_buildings(self):
        misc_tests.test_path_exists(self, misc.SHARE_BUILDINGS_2019)
        df, total_ls = DL.load_share_buildings(misc.SHARE_BUILDINGS_2019)

        # test if file is empty
        self.assertIsNotNone(df)
        self.assertIsNotNone(total_ls)


class Test_InputLoader(unittest.TestCase):
    # assure that the tabula table is loaded and still in it's path
    def test_load_param(self):
        misc_tests.test_path_exists(self, misc.SCENARIOS)
        # check the file first
        with open(misc.SCENARIOS) as file:
            params = json.load(file)
        # test if file is empty
        self.assertIsNotNone(params)
        keys = [
            '2020', '2030', '2040', '2050', '2060', 'interpolation',
            'divergence'
        ]

        # check divergence
        def divergence_checker(div):
            self.assertLessEqual(div, 1)
            self.assertGreaterEqual(div, 0)

        for scenario in params.keys():
            # check if given parameters are in dataframe
            check_column_list = params[scenario].keys()
            parameters = [
                'restoration_rate', 'restoration_deep_amb', 'restoration_sfh',
                'restoration_th', 'restoration_mfh', 'restoration_ab',
                'demolition_rate_min', 'demolition_sfh', 'demolition_th',
                'demolition_mfh', 'demolition_ab', 'new_building_rate_min',
                'new_building_share_sfh', 'new_building_share_th',
                'new_building_share_mfh', 'new_building_deep_amb',
                'living_space_pc'
            ]
            misc_tests.check_in(self, parameters, check_column_list)

            # check if given keys are in dataframe
            for vals in params[scenario].values():
                misc_tests.check_in(self, keys, vals)
                # ensure floats at the years
                if vals in ['2020', '2030', '2040', '2050', '2060']:
                    self.assertTrue(isinstance(params[scenario][vals], float))
                # ensure correct declarations for interpolation
                elif vals == 'interpolation':
                    interpolations_allowed = ['linear', 'exponential']
                    misc_tests.check_in(self, params[scenario][vals],
                                        interpolations_allowed)
                elif vals == 'divergence':
                    if (isinstance(params[scenario][vals], str)):
                        self.assertIn(',', params[scenario][vals])
                        div_factor = params[scenario][vals].split(',')
                        div_factor = [float(i.strip()) for i in div_factor]
                        for div2 in div_factor:
                            divergence_checker(div2)
                    else:
                        divergence_checker(params[scenario][vals])

        # second check the loaded dict
        scen_params, years = IL.load_param(misc.SCENARIOS)
        for key, value in scen_params.items():
            self.assertIn(key, params.keys())
            for ke, val in value.items():
                self.assertIn(ke, parameters)
                self.assertIsInstance(val, list)
                for va in val:
                    self.assertIsNotNone(va)

    def test_load_hyperparameter(self):
        misc_tests.test_path_exists(self, misc.HYPERPARAMETER)
        hyper = IL.load_hyperparameter(misc.HYPERPARAMETER)
        keys = [
            'bev_variant', 'scenario', 'restauration_building_type bias',
            'second_amb_restauration', 'output_folder'
        ]
        for key, value in hyper.items():
            self.assertIn(key, keys)
            self.assertIsNotNone(value)


if __name__ == '__main__':
    unittest.main()
