import sys
import os

# this adds the root directory to our path to be able to load data from root
FOLDER_PATH = os.path.dirname(__file__)
sys.path.insert(1, os.path.join(FOLDER_PATH, '..'))


# little  helper function to check elements against list
def check_in(self, check_list, check_against):
    for check_element in check_list:
        self.assertIn(check_element, check_against,
                      "{} not in dataframe".format(check_element))


def test_path_exists(self, *args):
    # on object creation check if all relevant paths are valid
    for arg in args:
        self.assertTrue(os.path.exists(arg))
