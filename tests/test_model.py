"""

@author: guuug
"""

import unittest
import os
import pandas as pd
import misc_tests
import model
import scripts.inputs as inputs
import scripts.misc as misc

# GLOBALS

DL = inputs.DataLoader()
IL = inputs.InputLoader()


# TODO: test model with different parameter configurations (all 0, all 1, ...)
class Test_Model(unittest.TestCase):
    def test_heating_demand(self):
        misc_tests.test_path_exists(self, misc.HYPERPARAMETER,
                                    misc.DEMOGRAPHIC_DEVELOPEMENT, misc.TABULA,
                                    misc.SHARE_BUILDINGS_2019)
        hyperparameter = IL.load_hyperparameter(misc.HYPERPARAMETER)
        scen_params, years = IL.load_param(misc.SCENARIOS)
        true_years = {
            'years_19-60': [
                2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028,
                2029, 2030, 2031, 2032, 2033, 2034, 2035, 2036, 2037, 2038,
                2039, 2040, 2041, 2042, 2043, 2044, 2045, 2046, 2047, 2048,
                2049, 2050, 2051, 2052, 2053, 2054, 2055, 2056, 2057, 2058,
                2059, 2060
            ],
            'years_20-60': [
                2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029,
                2030, 2031, 2032, 2033, 2034, 2035, 2036, 2037, 2038, 2039,
                2040, 2041, 2042, 2043, 2044, 2045, 2046, 2047, 2048, 2049,
                2050, 2051, 2052, 2053, 2054, 2055, 2056, 2057, 2058, 2059,
                2060
            ]
        }
        self.assertDictEqual(true_years, years)

        _ = model.main(False)
        if 'all' in hyperparameter['scenario']:
            chosen_scenarios = list(scen_params.keys())
        else:
            chosen_scenarios = hyperparameter['scenario']
        chosen_scenarios.sort()

        headers = [
            'total_sh_need', 'spec_sh_need', 'total_hot_water_need',
            'total_heat_need', 'high_sh_need', 'middle_sh_need', 'low_sh_need',
            'EFH_A_001_sh_need', 'EFH_A_002_sh_need', 'EFH_A_003_sh_need',
            'EFH_A_sh_need', 'EFH_B_001_sh_need', 'EFH_B_002_sh_need',
            'EFH_B_003_sh_need', 'EFH_B_sh_need', 'EFH_C_001_sh_need',
            'EFH_C_002_sh_need', 'EFH_C_003_sh_need', 'EFH_C_sh_need',
            'EFH_D_001_sh_need', 'EFH_D_002_sh_need', 'EFH_D_003_sh_need',
            'EFH_D_sh_need', 'EFH_E_001_sh_need', 'EFH_E_002_sh_need',
            'EFH_E_003_sh_need', 'EFH_E_sh_need', 'EFH_F_001_sh_need',
            'EFH_F_002_sh_need', 'EFH_F_003_sh_need', 'EFH_F_sh_need',
            'EFH_G_001_sh_need', 'EFH_G_002_sh_need', 'EFH_G_003_sh_need',
            'EFH_G_sh_need', 'EFH_H_001_sh_need', 'EFH_H_002_sh_need',
            'EFH_H_003_sh_need', 'EFH_H_sh_need', 'EFH_I_001_sh_need',
            'EFH_I_002_sh_need', 'EFH_I_003_sh_need', 'EFH_I_sh_need',
            'EFH_J_001_sh_need', 'EFH_J_002_sh_need', 'EFH_J_003_sh_need',
            'EFH_J_sh_need', 'EFH_K_001_sh_need', 'EFH_K_002_sh_need',
            'EFH_K_003_sh_need', 'EFH_K_sh_need', 'EFH_L_001_sh_need',
            'EFH_L_002_sh_need', 'EFH_L_003_sh_need', 'EFH_L_sh_need',
            'GMH_B_001_sh_need', 'GMH_B_002_sh_need', 'GMH_B_003_sh_need',
            'GMH_B_sh_need', 'GMH_C_001_sh_need', 'GMH_C_002_sh_need',
            'GMH_C_003_sh_need', 'GMH_C_sh_need', 'GMH_D_001_sh_need',
            'GMH_D_002_sh_need', 'GMH_D_003_sh_need', 'GMH_D_sh_need',
            'GMH_E_001_sh_need', 'GMH_E_002_sh_need', 'GMH_E_003_sh_need',
            'GMH_E_sh_need', 'GMH_F_001_sh_need', 'GMH_F_002_sh_need',
            'GMH_F_003_sh_need', 'GMH_F_sh_need', 'MFH_A_001_sh_need',
            'MFH_A_002_sh_need', 'MFH_A_003_sh_need', 'MFH_A_sh_need',
            'MFH_B_001_sh_need', 'MFH_B_002_sh_need', 'MFH_B_003_sh_need',
            'MFH_B_sh_need', 'MFH_C_001_sh_need', 'MFH_C_002_sh_need',
            'MFH_C_003_sh_need', 'MFH_C_sh_need', 'MFH_D_001_sh_need',
            'MFH_D_002_sh_need', 'MFH_D_003_sh_need', 'MFH_D_sh_need',
            'MFH_E_001_sh_need', 'MFH_E_002_sh_need', 'MFH_E_003_sh_need',
            'MFH_E_sh_need', 'MFH_F_001_sh_need', 'MFH_F_002_sh_need',
            'MFH_F_003_sh_need', 'MFH_F_sh_need', 'MFH_G_001_sh_need',
            'MFH_G_002_sh_need', 'MFH_G_003_sh_need', 'MFH_G_sh_need',
            'MFH_H_001_sh_need', 'MFH_H_002_sh_need', 'MFH_H_003_sh_need',
            'MFH_H_sh_need', 'MFH_I_001_sh_need', 'MFH_I_002_sh_need',
            'MFH_I_003_sh_need', 'MFH_I_sh_need', 'MFH_J_001_sh_need',
            'MFH_J_002_sh_need', 'MFH_J_003_sh_need', 'MFH_J_sh_need',
            'MFH_K_001_sh_need', 'MFH_K_002_sh_need', 'MFH_K_003_sh_need',
            'MFH_K_sh_need', 'MFH_L_001_sh_need', 'MFH_L_002_sh_need',
            'MFH_L_003_sh_need', 'MFH_L_sh_need', 'RH_B_001_sh_need',
            'RH_B_002_sh_need', 'RH_B_003_sh_need', 'RH_B_sh_need',
            'RH_C_001_sh_need', 'RH_C_002_sh_need', 'RH_C_003_sh_need',
            'RH_C_sh_need', 'RH_D_001_sh_need', 'RH_D_002_sh_need',
            'RH_D_003_sh_need', 'RH_D_sh_need', 'RH_E_001_sh_need',
            'RH_E_002_sh_need', 'RH_E_003_sh_need', 'RH_E_sh_need',
            'RH_F_001_sh_need', 'RH_F_002_sh_need', 'RH_F_003_sh_need',
            'RH_F_sh_need', 'RH_G_001_sh_need', 'RH_G_002_sh_need',
            'RH_G_003_sh_need', 'RH_G_sh_need', 'RH_H_001_sh_need',
            'RH_H_002_sh_need', 'RH_H_003_sh_need', 'RH_H_sh_need',
            'RH_I_001_sh_need', 'RH_I_002_sh_need', 'RH_I_003_sh_need',
            'RH_I_sh_need', 'RH_J_001_sh_need', 'RH_J_002_sh_need',
            'RH_J_003_sh_need', 'RH_J_sh_need', 'RH_K_001_sh_need',
            'RH_K_002_sh_need', 'RH_K_003_sh_need', 'RH_K_sh_need',
            'RH_L_001_sh_need', 'RH_L_002_sh_need', 'RH_L_003_sh_need',
            'RH_L_sh_need', 'dem_sh_need_decrease'
        ]

        for current_scen in chosen_scenarios:
            hd_path = os.path.join(hyperparameter['output_folder'],
                                   current_scen, 'heat_demand_dev.xlsx')
            heat_demand = pd.read_excel(hd_path)
            header_names = heat_demand.columns.values.tolist()
            misc_tests.check_in(self, headers, header_names)
            index_names = heat_demand['Unnamed: 0'].tolist()
            self.assertEqual(index_names, true_years['years_19-60'])


if __name__ == '__main__':
    unittest.main()
