import gi
import os
import json
import copy
import pandas as pd

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class ScenariosWindow(Gtk.Window):
    def __init__(self):
        super().__init__(title='Manage Scenarios')
        self.set_border_width(10)
        self.homepath = os.path.dirname(__file__)
        self.scenarios_path = os.path.join(self.homepath, 'scenarios.xlsx')
        excel_data_df = pd.read_excel(self.scenarios_path,
                                      sheet_name=None,
                                      index_col=0)

        # transfer to json and then make it changeable (via .to_json())
        self.parameters = {
            scen: json.loads(excel_data_df[scen].to_json())
            for scen in excel_data_df.keys()
        }

        # Setting up the self.grid in which the elements are to be positioned
        headerbar = Gtk.HeaderBar()
        headerbar.set_show_close_button(True)
        self.set_titlebar(headerbar)
        expl_txt = '1. Choose Scenario.\
            2. Change its name and its values.\
            3. click "Save Scenario"\
            '

        headerbar.pack_start(Gtk.Label(label=expl_txt))

        rm_scen = Gtk.Button(label='Remove Scenario')
        rm_scen.connect('clicked', self.on_clicked_rm_scen)
        headerbar.pack_start(rm_scen)

        save_scen = Gtk.Button(label='Save Scenario')
        save_scen.connect('clicked', self.on_clicked_save_scen)
        headerbar.pack_start(save_scen)

        scenario_label = Gtk.Label(label='Scenario')

        first_scenario = self.parameters[list(self.parameters.keys())[0]]
        self.template = {
            k: {kk: 0
                for kk in first_scenario[k].keys()}
            for k in first_scenario.keys()
        }

        self.entry_organizer = copy.deepcopy(self.template)
        self.grid = Gtk.Grid()
        self.grid.set_column_homogeneous(True)
        self.grid.set_row_homogeneous(True)
        self.add(self.grid)
        for idx, (k, v) in enumerate(self.template.items()):
            self.grid.attach(Gtk.Label(label=k), idx + 1, 1, 1, 1)
            for i, (kk, vv) in enumerate(v.items()):
                if idx == 0:
                    self.grid.attach(Gtk.Label(label=kk), 0, i + 2, 1, 1)
                entry = Gtk.Entry()
                self.grid.attach(entry, idx + 1, i + 2, 1, 1)
                self.entry_organizer[k][kk] = entry

        self.scenario_input = Gtk.ComboBoxText.new_with_entry()
        self.scenario_input.set_entry_text_column(0)
        self.scenario_input.connect("changed", self.on_scenario_input_changed)
        for scen in list(self.parameters.keys()):
            self.scenario_input.append_text(scen)
        self.scenario_input.set_active(0)

        headerbar.pack_start(scenario_label)
        headerbar.pack_start(self.scenario_input)

        self.show_all()

    def on_load_scen_changed(self, load_scen):
        with open(load_scen.get_filename()) as file:
            self.parameters = json.load(file)
            self.scenarios_path = os.path.join(load_scen.get_current_folder(),
                                               load_scen.get_filename())
            # print(self.scenarios_path)

    def write_to_xlsx(self):
        df = [
            pd.DataFrame.from_dict(self.parameters[scenario])
            for scenario in self.parameters.keys()
        ]

        with pd.ExcelWriter(self.scenarios_path) as writer:
            for idx, scenario in enumerate(self.parameters.keys()):
                df[idx].to_excel(writer, sheet_name=scenario)

    def on_clicked_save_scen(self, save_scen):
        scenario_name = self.scenario_input.get_active_text()
        if scenario_name not in self.parameters.keys():
            self.parameters[scenario_name] = self.template
            self.scenario_input.append_text(scenario_name)
        for row, cols in self.parameters[scenario_name].items():
            for col in cols.keys():
                self.parameters[scenario_name][row][
                    col] = self.entry_organizer[row][col].get_text()
        self.write_to_xlsx()

    def on_clicked_rm_scen(self, rm_scen):
        scenario_name = self.scenario_input.get_active_text()
        scenario_id = self.scenario_input.get_active()
        if scenario_name in self.parameters.keys():
            self.parameters.pop(scenario_name)
        self.write_to_xlsx()
        self.scenario_input.remove(scenario_id)
        self.scenario_input.set_active(0)

    def on_scenario_input_changed(self, scenario_input):
        scenario = self.scenario_input.get_active_text()
        if scenario in self.parameters.keys():
            for idx, (k, v) in enumerate(self.parameters[scenario].items()):
                for i, (kk, vv) in enumerate(v.items()):
                    self.entry_organizer[k][kk].set_text(str(vv))


win = ScenariosWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
