"""File to be run to start the housing model. Contains the most relevant functions.

@author: guuug
"""

# IMPORTS

import os
import pandas as pd
from multiprocessing import Pool

import scripts.r_d_nb as r_d_nb
import scripts.plots as plots
import scripts.misc as misc


@misc.execution_time
def heating_demand(
    tabula_heat,
    tabula_buildings,
    all_sh_need,
    all_hw_need,
    all_ls,
    years_19_60,
    all_d_col,
):
    """Calculate the heating demand for each building type.

    Each Building Type has a specific heat need per square meter. From this find out
    the total space heat, the high space heat need (>=120), the low (<90) and the
    middle between 90 and 120.

    Parameters:
        - tabula_heat: central dataframe for space heat need calculations.,
        - tabula_buildings: central dataframe for living space calculations.,
        - all_sh_need: list, column names of space_heat_need from tabula_heat over all
          calculated years,
        - all_hw_need: list, column names of hot_water_need from tabula_heat over all
          calculated years,
        - all_ls: list, column names of living_space from tabula_buildings over all
          calculated years,
        - years_19_60: list of ints in range 2019 - 2060,
        - all_d_col: list, column names of demolition_area from tabula_buildings over
          all calculated years,

    Returns:
        - df_heat_demand: dataframe with all heat_demand - related entries in order to
          save them to disk
    """
    df_heat_demand = pd.DataFrame(data={}, index=years_19_60)
    sh_need_m2 = tabula_heat["space_heat_need_m2"]
    high_map = sh_need_m2 >= 120
    middle_map = (sh_need_m2 >= 90) & (sh_need_m2 < 120)
    low_map = sh_need_m2 < 90

    def get_sh_need(map):
        return [tabula_heat[sh_need].loc[map] for sh_need in all_sh_need]

    high_sh_need = get_sh_need(high_map)
    middle_sh_need = get_sh_need(middle_map)
    low_sh_need = get_sh_need(low_map)

    # total sum
    total_ls = [sum(tabula_buildings[new_ls]) for new_ls in all_ls]
    df_heat_demand.loc[:, "total_ls"] = total_ls
    total_sh_need = [sum(tabula_heat[sh_need]) for sh_need in all_sh_need]
    df_heat_demand.loc[:, "total_sh_need"] = total_sh_need
    df_heat_demand.loc[:, "spec_sh_need"] = [
        t_sh / t_ls for t_sh, t_ls in zip(total_sh_need, total_ls)
    ]
    total_hw_need = [sum(tabula_heat[hw_need]) for hw_need in all_hw_need]
    df_heat_demand.loc[:, "total_hot_water_need"] = total_hw_need
    df_heat_demand.loc[:, "total_heat_need"] = [
        t_sh + t_hw for t_sh, t_hw in zip(total_sh_need, total_hw_need)
    ]
    # sum (>=120)
    df_heat_demand.loc[:, "high_sh_need"] = [sum(high_sh) for high_sh in high_sh_need]
    # sum (<120 >=90)
    df_heat_demand.loc[:, "middle_sh_need"] = [
        sum(middle_sh) for middle_sh in middle_sh_need
    ]
    # sum (<90)
    df_heat_demand.loc[:, "low_sh_need"] = [sum(low_sh) for low_sh in low_sh_need]

    # new buildings sh need
    # translation from german tabula identifier to english
    trans_bt = {"SFH": "EFH", "TH": "RH", "AB": "GMH", "MFH": "MFH"}
    trans_bc = {
        "01": "A",
        "02": "B",
        "03": "C",
        "04": "D",
        "05": "E",
        "06": "F",
        "07": "G",
        "08": "H",
        "09": "I",
        "10": "J",
        "11": "K",
        "12": "L",
    }

    def get_bc_bv_sh(row):
        ident = row["identifier"].split(".")
        bc = trans_bt[ident[2]] + "_" + trans_bc[ident[3]]
        bv = ident[7]
        sh = [row[f"space_heat_need_{year}"] for year in years_19_60]
        return bc, bv, sh

    sh_need_view_cols = ["identifier"] + all_sh_need
    bc_bv_sh = list(
        tabula_heat.loc[:, sh_need_view_cols].apply(
            lambda row: get_bc_bv_sh(row), axis=1
        )
    )

    # sort to have 001, 002, 003 one after the other for each building type
    # bc_bv_sh.sort(key=lambda y: y[0])
    col_bc = [f"{bc}_sh_need" for bc, _, _ in bc_bv_sh]
    sh_len_zero = [0 for _ in range(len(bc_bv_sh[0][-1]))]
    dict_bc_sh = {bc_sh: sh_len_zero for bc_sh in col_bc}
    for bc_sh, sh in zip(col_bc, bc_bv_sh):
        new_sum = [el + le for el, le in zip(sh[-1], dict_bc_sh[bc_sh])]
        dict_bc_sh[bc_sh] = new_sum

    dict_bc_bv_sh = {f"{bc}_{bv}_sh_need": sh for bc, bv, sh in bc_bv_sh}

    # appending the dictionaries (needs python >= 3.9)
    # dict_all = {'years': years_19_60} | dict_bc_bv_sh | dict_bc_sh

    # appending dictionaries with python < 3.9
    dict_all = {"years": years_19_60, **dict_bc_bv_sh, **dict_bc_sh}

    df_bc_bv_sh = pd.DataFrame.from_dict(dict_all)
    df_bc_bv_sh.set_index("years", inplace=True)
    df_bc_bv_sh = df_bc_bv_sh.reindex(sorted(df_bc_bv_sh.columns), axis=1)

    df_heat_demand = df_heat_demand.join(df_bc_bv_sh)

    offset = 0
    for idx, current_year in enumerate(years_19_60):
        if current_year > 2019:
            offset = df_heat_demand.loc[current_year - 1, "dem_sh_need_decrease"]
            df_heat_demand.loc[current_year, "dem_sh_need_decrease"] = (
                sum(sh_need_m2.mul(tabula_buildings[all_d_col[idx - 1]], fill_value=0))
                + offset
            )
        else:
            df_heat_demand.loc[current_year, "dem_sh_need_decrease"] = 0

    return df_heat_demand


def housing_model(tabula_buildings, tabula_heat, tabula_sealing, params, current_scen):
    """Start the housing model for one specific scenario.

    Run through the years and calculate the needed new columns and add to DataFrames.

    Parameters:
        - tabula_buildings: central dataframe for living space calculations.,
        - tabula_heat: central dataframe for space heat need calculations.,
        - tabula_sealing: central dataframe for sealing calculations.,
        - params: parameter class to access settings for the model,
        - current_scen: string, model scenario for scenario-specific-settings

    Returns:
        - misc.METHODS_TIME: running time for evaluation
    """
    params_current_scen = params.scen_params[current_scen]
    save_path = os.path.join(params.hyperparameter["output_folder"], current_scen)
    if not os.path.exists(save_path):
        os.makedirs(save_path)

    tabula_ls = tabula_buildings.loc[:, ["identifier", "living_space_2019"]]
    tabula_sealing_nb = tabula_sealing.copy()

    def space_heat_entries(
        tabula_heat, tabula_buildings, current_ls, space_heat_need, hot_water_need
    ):
        """Calculate space heat entries.

        Parameters:
            - tabula_heat: central dataframe for space heat need calculations.,
            - tabula_buildings: central dataframe for living space calculations.,
            - current_ls: column name for tabula_buildings of current year living space,
            - space_heat_need: column name for tabula_heat of current year,
            - hot_water_need: column name for tabula_heat of current year

        Returns:
            - tabula_heat: central dataframe for space heat need calculations.
        """
        tabula_heat.loc[:, space_heat_need] = (
            tabula_heat["space_heat_need_m2"] * tabula_buildings[current_ls]
        )
        tabula_heat.loc[:, hot_water_need] = (
            tabula_heat["hot_water_need_m2"] * tabula_buildings[current_ls]
        )
        return tabula_heat

    all_ls, all_sh_need, all_hw_need, all_d_col = [], [], [], []
    # start with 2020 until 2060
    for i, current_year in enumerate(params.years["years_20-60"]):
        # new column names
        calc_ls = f"calc_living_space_{current_year}"
        new_ls = f"living_space_{current_year}"
        old_ls = f"living_space_{current_year - 1}"
        r_col = f"restoration_area_{current_year}"
        d_col = f"demolition_area_{current_year}"
        nb_col = f"new_building_area_{current_year}"
        space_heat_need = f"space_heat_need_{current_year}"
        hot_water_need = f"hot_water_need_{current_year}"
        sealing_total = f"soil_sealing_total{current_year}"
        sealing_nb = f"nb_soil_selaing{current_year}"

        # create entries for 2019
        if i == 0:
            old_sh_need = f"space_heat_need_{current_year - 1}"
            old_hw_need = f"hot_water_need_{current_year - 1}"

            tabula_heat = space_heat_entries(
                tabula_heat, tabula_buildings, old_ls, old_sh_need, old_hw_need
            )

            all_ls.append(old_ls)
            all_sh_need.append(old_sh_need)
            all_hw_need.append(old_hw_need)

        def r_d_nb_area_i(r_d_rate):
            return params_current_scen[r_d_rate][i] * sum(tabula_buildings[old_ls])
            # params_current_scen['total_living_space'][f'{2019 + i}']

        r_area_i = r_d_nb_area_i("restoration_rate")
        r_deep_amb = params_current_scen["restoration_deep_amb"][i]

        tabula_buildings, dists_r = r_d_nb.restauration(
            r_area_i,
            r_deep_amb,
            params,
            current_year,
            tabula_buildings,
            old_ls,
            calc_ls,
            r_col,
        )

        d_area_i = r_d_nb_area_i("demolition_rate")

        tabula_buildings, dists_d = r_d_nb.demolition(
            d_area_i, params, current_year, tabula_buildings, calc_ls, d_col
        )

        if debug_flag:

            def save_df(dist_save, dist_save_name):
                with pd.ExcelWriter(os.path.join(save_path, dist_save_name)) as writer:
                    for sheet_name, df in dist_save.items():
                        df.to_excel(writer, sheet_name=sheet_name)

            save_dict = {"r": dists_r, "d": dists_d}
            for r_d, dist in save_dict.items():
                for bv in [params.bv_001, params.bv_002, params.bv_003]:
                    if bv in dist.keys():
                        save_df(
                            dist[bv][f"dist_{r_d}_{bv}"],
                            dist[bv][f"dist_save_name_{bv}"],
                        )

        nb_area_i = r_d_nb_area_i("new_building_rate")
        nb_amb = params_current_scen["new_building_deep_amb"][i]
        nb_spec_areas = {
            "nb_sfh_area": params_current_scen["new_building_share_sfh"][i] * nb_area_i,
            "nb_th_area": params_current_scen["new_building_share_th"][i] * nb_area_i,
            "nb_mfh_area": params_current_scen["new_building_share_mfh"][i] * nb_area_i,
        }
        tabula_buildings = r_d_nb.new_buildings(
            nb_area_i, nb_amb, nb_spec_areas, tabula_buildings, calc_ls, new_ls, nb_col
        )
        tabula_buildings.drop(calc_ls, axis=1, inplace=True)

        tabula_heat = space_heat_entries(
            tabula_heat, tabula_buildings, new_ls, space_heat_need, hot_water_need
        )

        tabula_sealing[sealing_total] = (
            tabula_buildings[new_ls] / tabula_sealing["energy_reference_area"]
        ) * tabula_sealing["sealing"]

        tabula_sealing_nb[sealing_nb] = (
            tabula_buildings[nb_col] / tabula_sealing["energy_reference_area"]
        ) * tabula_sealing["sealing"]

        tabula_ls[new_ls] = tabula_buildings[new_ls]
        # saving col names that are needed for heating demand
        all_ls.append(new_ls)
        all_hw_need.append(hot_water_need)
        all_sh_need.append(space_heat_need)
        all_d_col.append(d_col)
        print(f"processed year {current_year} for {current_scen} scenario")

    years_19_60 = params.years["years_19-60"]

    df_heat_demand = heating_demand(
        tabula_heat,
        tabula_buildings,
        all_sh_need,
        all_hw_need,
        all_ls,
        years_19_60,
        all_d_col,
    )

    # adding rows to tabula_ls, calculate the living_space per capita per year
    bev_per_row = {"identifier": "population"}
    ls_per_year = {"identifier": "ls per capita"}
    for x in range(42):
        ls_x = f"living_space_{2019 + x}"
        bev_per_row[ls_x] = params.bev_all[x]
        ls_per_year[ls_x] = sum(tabula_ls[ls_x]) / params.bev_all[x]
    tabula_ls = pd.concat([tabula_ls, pd.DataFrame.from_records([bev_per_row])])
    tabula_ls = pd.concat([tabula_ls, pd.DataFrame.from_records([ls_per_year])])

    with pd.ExcelWriter(os.path.join(save_path, params.results_name)) as writer:
        tabula_buildings.to_excel(writer, sheet_name="buildings_stock")
        tabula_heat.to_excel(writer, sheet_name="heat_stock")
        tabula_sealing.to_excel(writer, sheet_name="sealing_stock")
        tabula_sealing_nb.to_excel(writer, sheet_name="nb_sealing_stock")
        tabula_ls.to_excel(writer, sheet_name="living_space_stock")
        df_heat_demand.to_excel(writer, sheet_name="heat_demand")

    plots.plot_heat_demand(df_heat_demand, years_19_60, save_path, current_scen)

    return misc.METHODS_TIME


@misc.execution_time
def main(debug_flag, dis_mon_prot_flag):
    """Start function which runs the (parallel) execution of the model.

    1. Load the parameters and the initial DataFrames (buildings, heat, sealing).
    2. Plot the population overview to file.
    3. Depending on chosen number of scenarios (in Hyperparameter file), start one
       process for each chosen scenario (up to os.cpu_count() processes in parallel).
    4. Plot multi-scenario figures and initial parameter settings.

    Parameters:
        - debug_flag: if set, outputs runtime information on console and saves more
          information to csv during run.
        - dis_mon_prot_flag: if set, monument protection will be disabled during run.
    """
    # TODO: Change xlsx to ods everywhere - only use open formats

    params = misc.Parameter(debug_flag, dis_mon_prot_flag)
    tabula_buildings, tabula_heat, tabula_sealing = params.get_tabula()

    plots.plot_population_all(params)
    processes = min(len(params.chosen_scenarios), os.cpu_count())
    with Pool(processes=processes) as pool:
        time_overview = pool.starmap(
            housing_model,
            [
                [tabula_buildings, tabula_heat, tabula_sealing, params, scen]
                for scen in params.chosen_scenarios
            ],
        )
    tmp = {}
    for scen_times in time_overview:
        tmp = misc.add_to_dict(tmp, scen_times)
        computation_time = tmp
    for k in computation_time.keys():
        computation_time[k] /= len(time_overview)

    print("finished calculations, creating scenario comparison plots")
    scen_paths = [
        os.path.join(params.hyperparameter["output_folder"], scen, params.results_name)
        for scen in params.chosen_scenarios
    ]
    if len(params.chosen_scenarios) > 1:
        plots.plot_scenarios(scen_paths, params)
    plots.plot_params(scen_paths, params)

    output_path = os.path.join(
        os.path.dirname(__file__), params.hyperparameter["output_folder"]
    )
    print(f"Done calculating and plotting. Find the outputs in {output_path}")
    return computation_time


if __name__ == "__main__":
    """Starts the main function when the file is being called."""
    debug_flag, dis_mon_prot_flag = misc.parse_arguments()
    computation_time = main(debug_flag, dis_mon_prot_flag)

    computation_time = misc.add_to_dict(computation_time, misc.METHODS_TIME)
    computation_time["complete_model"] = computation_time.pop("main")
    misc.print_debug(f"time needed in seconds (mean values): {computation_time}")
