"""All Plots for Housing Model.

@author: guuug
"""

import os
import pandas as pd

import matplotlib

import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from scripts.misc import execution_time

matplotlib.use("Agg")


def gwh_to_twh(gwh):
    """Divide gigawatthours by 1000 to get Terwatthours."""
    return gwh / 1000


@execution_time
def plot_heat_demand(df_heat_demand, years, save_path, current_scen):
    """Heat demand plots.

    Plot: Heat demand comparison (high, middle, low space heat need).
    Plot: Space heat demand comparison, total space heat need.
    Plot: Space heat demand comparison, total and specific space heat need.
    Plot: Heat need of building+aging classes.

    Parameters:
        - df_heat_demand: dataframe with all heat_demand - related entries in order to
          save them to disk,
        - years: list of ints in range 2019 - 2060,
        - save_path: string, path to save images to disk,
        - current_scen: string, model scenario for scenario-specific-settings
    """

    def set_title(title):
        return f"{current_scen}: {title}"

    high_sh_need = gwh_to_twh(df_heat_demand["high_sh_need"])
    middle_sh_need = gwh_to_twh(df_heat_demand["middle_sh_need"])
    low_sh_need = gwh_to_twh(df_heat_demand["low_sh_need"])
    plt.figure()
    # Plot x-labels, y-label and data
    plt.plot([], [], color="blue", label="low_sh_need")
    plt.plot([], [], color="orange", label="middle_sh_need")
    plt.plot([], [], color="brown", label="high_sh_need")

    plt.stackplot(
        years,
        low_sh_need,
        middle_sh_need,
        high_sh_need,
        baseline="zero",
        colors=["blue", "orange", "brown"],
    )
    plt.legend(bbox_to_anchor=(1.05, 1.0), loc="upper left")
    title = set_title("Heat demand comparison")
    plt.title(title)
    plt.xlabel("years")
    plt.ylabel("space heat need in TWh")

    plt.savefig(os.path.join(save_path, title), bbox_inches="tight")
    # plt.show()

    # Raumwärme und Warmwasser bedarf
    # total_sh_need = df_heat_demand['total_sh_need']
    total_hot_water_need = gwh_to_twh(df_heat_demand["total_hot_water_need"])
    total_heat_need = gwh_to_twh(df_heat_demand["total_heat_need"])

    plt.figure()
    # Plot x-labels, y-label and data
    # plt.plot([], [], color='blue', label='total_sh_need')
    plt.plot([], [], color="orange", label="total_hot_water_need")
    plt.plot([], [], color="brown", label="total_heat_need")

    plt.stackplot(
        years,
        total_hot_water_need,
        total_heat_need,
        baseline="zero",
        colors=["orange", "brown"],
    )
    plt.legend(bbox_to_anchor=(1.05, 1.0), loc="upper left")
    title = set_title("Space heat demand comparison")
    plt.title(title)
    plt.xlabel("years")
    plt.ylabel("total space heat need in TWh")
    plt.savefig(os.path.join(save_path, title), bbox_inches="tight")
    # plt.show()

    # Raumwärmebedarf und spezifischer Wärmebedarf
    total_sh_need = list(df_heat_demand["total_sh_need"])
    spec_sh_need = list(df_heat_demand["spec_sh_need"])
    total_sh_need = [gwh_to_twh(tsh) for tsh in total_sh_need]

    fig, ax1 = plt.subplots()
    title = set_title("Space heat demand comparison")
    plt.title(title)
    color = "tab:red"
    ax1.set_xlabel("years")
    ax1.set_ylabel("space heat need in TWh", color=color)
    ax1.tick_params(axis="y", labelcolor=color)
    offset = 20
    limit_ax1 = total_sh_need[0] + offset
    ax1.set_ylim(0, limit_ax1)
    ax1.plot(years, total_sh_need, color=color)
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    limit_ax2 = (spec_sh_need[0] / total_sh_need[0]) * limit_ax1

    color = "tab:blue"
    ax2.tick_params(axis="y", labelcolor=color)
    ax2.set_ylim(0, limit_ax2)
    ax2.plot(years, spec_sh_need, color=color)
    ax2.set_ylabel("specific space heat need in kWh/m2", color=color)
    fig.tight_layout()
    # plt.legend()
    plt.savefig(os.path.join(save_path, title), bbox_inches="tight")
    # plt.show()

    #   Wärmebedarf
    plt.figure()
    title = set_title("Heat need of building+aging classes")
    plt.title(title)
    plt.xlabel("years")
    plt.ylabel("space heat need in TWh")
    plt.plot([], [], color="red", label="A-J: 001")
    plt.plot([], [], color="orange", label="A-J: 002")
    plt.plot([], [], color="green", label="A-J: 003")
    plt.plot([], [], color="purple", label="L+K: 001")
    plt.plot([], [], color="blue", label="L+K: 002")
    plt.plot([], [], color="pink", label="L+K: 003")
    plt.plot([], [], color="gray", label="demolition")

    def get_existing_cols(house, ch, var_idx):
        try:
            return df_heat_demand[f"{house}_{ch}_{var_idx}_sh_need"]

        except KeyError:
            return [0 for _ in range(len(df_heat_demand))]
        except Exception() as e:
            print(e)

    to_stack = []
    for var_idx in ["001", "002", "003"]:
        tmp_to_stack = []
        for ch in ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"]:
            efh = get_existing_cols("EFH", ch, var_idx)
            mfh = get_existing_cols("MFH", ch, var_idx)
            rh = get_existing_cols("RH", ch, var_idx)
            gmh = get_existing_cols("GMH", ch, var_idx)
            tmp_to_stack.append(efh + mfh + rh + gmh)
        to_stack.append(sum(tmp_to_stack))
    for var_idx in ["001", "002", "003"]:
        tmp_to_stack = []
        for ch in ["K", "L"]:
            efh = get_existing_cols("EFH", ch, var_idx)
            mfh = get_existing_cols("MFH", ch, var_idx)
            rh = get_existing_cols("RH", ch, var_idx)
            gmh = get_existing_cols("GMH", ch, var_idx)
            tmp_to_stack.append(efh + mfh + rh + gmh)
        to_stack.append(sum(tmp_to_stack))

    # to_stack.append(-df_heat_demand['dem_sh_need_decrease'])
    to_stack = [gwh_to_twh(ts) for ts in to_stack]
    plt.stackplot(
        years,
        to_stack,
        baseline="zero",
        colors=["red", "orange", "green", "purple", "blue", "pink"],
    )
    plt.stackplot(
        years,
        gwh_to_twh(df_heat_demand["dem_sh_need_decrease"]),
        baseline="zero",
        colors=["gray"],
        hatch="x",
        alpha=0.8,
    )

    plt.legend(bbox_to_anchor=(1.05, 1.0), loc="upper left")
    plt.savefig(os.path.join(save_path, title), bbox_inches="tight")
    plt.close("all")
    # plt.show()

    # Plot 2: Wärmebedarf nach EFH, RH, MFH und Ab

    # Plot 3: Quadratmeter der unterschiedlichen Gebäude /
    # (müssen wir überlegen wonach wir unterteilen: EFH, RH, MFH und AB?)

    # Plot 4: Plotten wie viel Neubau, Abgang und Bestandsgebäude /
    # (eventuell Bestandsgebäude unterteilt in Buildingvariants)
    return -1


def save_to(title, output_folder, fold="scenario comparison"):
    """Save figure as png to path.

    Parameters:
        - title: string, image-title,
        - output_folder: string, folder to save image to,
        - fold: folder for either scenario or population comparison

    Returns:
        - complete path including title and .png ending
    """
    save_path = os.path.join(output_folder, fold)
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    return os.path.join(save_path, title + ".png")


def set_title(title, chosen_scenarios):
    """Set title for figure from multiple scenarios.

    Parameters:
        - title: string, title of figure,
        - chosen_scenarios: scenario-names to complement the title

    Returns:
        - title (String) including all scenarios.
    """
    scens = ""
    for idx, chos_scen in enumerate(chosen_scenarios):
        if idx == 0:
            scens = chos_scen
        else:
            scens = scens + " vs. " + chos_scen
    return f"{scens}: {title}"


@execution_time
def plot_scenarios(scen_paths, params):
    """Plot comparisons of run scenarios to file.

    Plot: Comparison of total living space.
    Plot: Comparison of per capita living space.
    Plot: Total heat demand comparison.
    Plot: Total vs. Specific heat demand comparison.
    Plot: Specific heat demand comparison.

    Paramters:
        - scen_paths: list, folder paths for each scenario,
        - params: parameter class to access settings for the model,
    """
    # plot total living space
    heat_demands = [
        pd.read_excel(path, sheet_name="heat_demand") for path in scen_paths
    ]
    fig, ax1 = plt.subplots()
    title = set_title("Comparison of total living space", params.chosen_scenarios)
    ax1.set_title(title)
    ax1.set_xlabel("years")
    ax1.set_ylabel("total living space in million m2")
    colors = ["red", "orange", "green", "purple", "blue", "pink"]
    max_ls = []
    line_types = ["-", "--", "-.", ":"]
    for idx, df_heat_demand in enumerate(heat_demands):
        # TODO: either load years as index or do sth. about it
        years = df_heat_demand["Unnamed: 0"]
        ls = df_heat_demand["total_ls"]
        ax1.plot(
            years,
            ls,
            color=colors[idx % len(colors)],
            label=params.chosen_scenarios[idx],
            linewidth=2,
            linestyle=line_types[idx % len(line_types)],
        )
        max_ls.append(max(list(ls)[0], list(ls)[-1]))

    offset = max(max_ls) / 10
    ax1.set_ylim(0, max(max_ls) + offset)
    ax1.legend(bbox_to_anchor=(1.05, 1.0), loc="upper left")
    plt.savefig(
        save_to(title, params.hyperparameter["output_folder"]), bbox_inches="tight"
    )
    # plt.show()

    fig, ax1 = plt.subplots()
    title = set_title("Comparison of per capita living space", params.chosen_scenarios)
    ax1.set_title(title)
    ax1.set_xlabel("years")
    ax1.set_ylabel("per capita living space in m2")
    max_ls = []
    line_types = ["-", "--", "-.", ":"]
    for idx, df_heat_demand in enumerate(heat_demands):
        # TODO: either load years as index or do sth. about it
        years = df_heat_demand["Unnamed: 0"]
        ls = df_heat_demand["total_ls"] / params.bev_all
        ax1.plot(
            years,
            ls,
            color=colors[idx % len(colors)],
            label=params.chosen_scenarios[idx],
            linewidth=2,
            linestyle=line_types[idx % len(line_types)],
        )
        max_ls.append(max(list(ls)[0], list(ls)[-1]))

    offset = max(max_ls) / 10
    ax1.set_ylim(0, max(max_ls) + offset)
    ax1.legend(bbox_to_anchor=(1.05, 1.0), loc="upper left")
    plt.savefig(
        save_to(title, params.hyperparameter["output_folder"]), bbox_inches="tight"
    )
    # plt.show()

    # comparison plot of average space heat scenario-wise
    fig, ax1 = plt.subplots()
    title = set_title("Total heat demand comparison", params.chosen_scenarios)
    plt.title(title)

    colors_total = ["lightcoral", "red", "orange", "gold", "khaki", "peru"]
    # value of 2019 - the same for all
    offset = 20
    limit_ax1 = gwh_to_twh(heat_demands[0]["total_sh_need"][0]) + offset
    ax1.set_xlabel("years")
    ax1.set_ylabel("total space heat need in TWh", color=colors_total[0])
    ax1.set_ylim(0, limit_ax1)
    ax1.tick_params(axis="y", labelcolor="tab:red")

    for idx, df_heat_demand in enumerate(heat_demands):
        total_sh_need = list(df_heat_demand["total_sh_need"])
        total_sh_need = [gwh_to_twh(tsh) for tsh in total_sh_need]

        ax1.plot(
            years,
            total_sh_need,
            color=colors_total[idx % len(heat_demands)],
            label=params.chosen_scenarios[idx] + " total",
        )
        ax1.legend(
            bbox_to_anchor=(1.12, 1), loc="upper left", title="Total space heat need"
        )

    plt.savefig(
        save_to(title, params.hyperparameter["output_folder"]), bbox_inches="tight"
    )
    # plt.show()

    title = set_title(
        "Total vs. Specific heat demand comparison", params.chosen_scenarios
    )
    plt.title(title)

    colors_specific = ["teal", "aqua", "deepskyblue", "steelblue", "navy", "blue"]

    def plot_specific(ax2):
        limit_ax2 = (
            heat_demands[0]["spec_sh_need"][0]
            / gwh_to_twh(heat_demands[0]["total_sh_need"][0])
        ) * limit_ax1
        ax2.tick_params(axis="y", labelcolor="tab:blue")
        ax2.set_ylim(0, limit_ax2)
        ax2.set_ylabel("specific heat need in kWh/m2", color=colors_specific[0])
        for idx, df_heat_demand in enumerate(heat_demands):
            spec_sh_need = list(df_heat_demand["spec_sh_need"])

            ax2.plot(
                years,
                spec_sh_need,
                color=colors_specific[idx % len(heat_demands)],
                label=params.chosen_scenarios[idx] + " specific",
            )
            ax2.legend(
                bbox_to_anchor=(1.12, 0.4), loc="upper left", title="Specific heat need"
            )

    ax2 = ax1.twinx()  # instantiate a second axes that shares the x-axis
    plot_specific(ax2)
    plt.savefig(
        save_to(title, params.hyperparameter["output_folder"]), bbox_inches="tight"
    )
    # plt.show()

    fig, ax1 = plt.subplots()
    title = set_title("Specific heat demand comparison", params.chosen_scenarios)
    plt.title(title)
    plot_specific(ax1)
    plt.savefig(
        save_to(title, params.hyperparameter["output_folder"]), bbox_inches="tight"
    )
    return -1


@execution_time
def plot_params(scen_paths, params):
    """Plot parameter comparisons.

    Parameters:
        - scen_paths: list, folder paths for each scenario,,
        - params: parameter class to access settings for the model,
    """

    def plot_key(key):
        fig, ax1 = plt.subplots()
        title = set_title(f"Comparison of {key} - parameter", params.chosen_scenarios)
        ax1.set_title(title)
        ax1.set_xlabel("years")
        # ax1.set_ylabel('')
        colors_tableau = list(mcolors.TABLEAU_COLORS)
        line_types = ["-", "--", "-.", ":"]

        def get_total_ls(tls, key):
            if key == "total_living_space":
                ls = [val for val in tls.values()]
                return ls[1:]  # omit 2019
            else:
                return tls

        max_y = 0
        offset = 0
        for idx, (scen, vals) in enumerate(params.scen_params.items()):
            # because total_ls is extra
            values = get_total_ls(vals[key], key)
            max_y = max(max_y, max(values))
            offset += sum(values) / len(params.years["years_20-60"])
            ax1.plot(
                params.years["years_20-60"],
                values,
                color=colors_tableau[idx % len(colors_tableau)],
                label=f"{scen}: {key}",
                linestyle=line_types[idx % len(line_types)],
            )
        offset = offset / len(params.scen_params.keys())
        offset = offset / 5
        ax1.set_ylim(0, max_y + offset)
        ax1.legend(bbox_to_anchor=(1.05, 1.0), loc="upper left")
        # plt.show()
        plt.savefig(
            save_to(
                title, params.hyperparameter["output_folder"], fold="params comparison"
            ),
            bbox_inches="tight",
        )
        plt.close()

    # keys_to_plot = ['restoration_rate', 'restoration_deep_amb',
    #                'demolition_rate', 'new_building_rate',
    #                'living_space_pc', 'total_living_space']
    keys_to_plot = params.scen_params[params.chosen_scenarios[0]].keys()
    for key in keys_to_plot:
        plot_key(key)
    plt.close("all")
    return -1


@execution_time
def plot_population_all(params):
    """Plot all population dev in one graph.

    Plot: Comparison of available population development assumptions.

    Parameters:
        - params: parameter class to access settings for the model,
    """
    pop_comp = "population comparison"
    pop_limit_max = 90
    pop_limit_min = 60
    css_colors = mcolors.CSS4_COLORS
    color_keys = list(css_colors.keys())
    color_keys = color_keys[10:]
    # all in one plot
    plt.figure(figsize=(10, 20))
    for idx, (k, v) in enumerate(params.dem_dev.items()):
        plt.plot(
            params.years["years_19-60"], v, label=k, color=css_colors[color_keys[idx]]
        )
    plt.plot(
        params.years["years_19-60"],
        params.dem_dev[params.hyperparameter["bev_variant"]],
        color="red",
        linestyle="--",
        linewidth=2,
        label=params.hyperparameter["bev_variant"],
    )
    plt.legend(bbox_to_anchor=(1.05, 1.0), loc="upper left")
    plt.ylim(pop_limit_min, pop_limit_max)
    title = "Comparison of available population development assumptions"
    plt.title(title)
    plt.xlabel("years")
    plt.ylabel("population number in million")

    plt.savefig(
        save_to(title, params.hyperparameter["output_folder"], fold=pop_comp),
        bbox_inches="tight",
    )
    return -1
