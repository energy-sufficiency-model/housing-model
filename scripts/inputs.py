"""Input and Data loading with DataLoader, InputLoader and RateCalculator classes.

@author: guuug
"""

from cmath import nan
import pandas as pd


class DataLoader:
    """Load data from tables and return it as dataframe."""

    # TODO: add to tests, check that sum == 1
    def load_dist_buildings(self, path_dist_build):
        """Minimalist dist buildings loader."""
        df = pd.read_excel(path_dist_build, index_col="rate").T
        return df

    def load_share_buildings(self, path_share_buildings):
        """Load share buildings and add percent living space column.

        Returns:
            - df: share buildings dataframe
            - total_living_space_2019: float, total living space of 2019
        """
        df = pd.read_excel(path_share_buildings)
        total_living_space_2019 = df["living_space_2019"].sum()
        relative_living_space = [
            x / total_living_space_2019 for x in df["living_space_2019"] if x is not nan
        ]
        # add a column called 'percent_living_space' and insert
        # calculated values
        df.loc[:, "percent_living_space"] = relative_living_space
        return df, total_living_space_2019

    def load_demographic_developement(self, path_demographic_dev):
        """Load demographic development for germany."""
        # TODO: check if the parameters should be changeable in a spreadsheet
        # somewhere and being called from there

        # parameters for calling the demographic developement
        columns = "A, C:AR"
        rows_start = 5
        rows_end = 35
        df = pd.read_excel(
            path_demographic_dev,
            usecols=columns,
            skiprows=rows_start,
            nrows=rows_end - rows_start,
        )
        df.rename(columns={"Unnamed: 0": "bev_variants"}, inplace=True)

        # dataframe in dictionary and change unit from thousand to million
        dem_dev = {}
        factor = 1000
        for i in range(len(df)):
            line = df.iloc[i]
            key = line["bev_variants"]
            value = list(line[1:])
            value = [val / factor for val in value]
            dem_dev[key] = value
        return dem_dev

    def load_tabula(self, path_tabula):
        """Load the tabula table as a dataframe."""
        # parameters for calling the tabula
        sheet_name = "DE Tables & Charts"
        # {own assigned name: [tabula name, class]}
        columns = {
            "identifier": ["F", str],
            "floor1": ["V", float],
            "floor2": ["W", float],
            "building_type": ["BB", str],
            "building_code": ["BC", str],
            "building_variant": ["BF", str],
            "energy_reference_area": ["BH", float],
            "space_heat_need_m2": ["BL", float],
            "hot_water_need_m2": ["BM", float],
            "heat_provided": ["BN", float],
            "hot_water_provided": ["BO", float],
        }

        # load data from columns_used in the correct format
        def get_type_cols(cols):
            dtype = {}
            columns = ""
            names = []
            for key, value in cols.items():
                dtype[key] = value[1]
                names.append(key)
                if columns == "":
                    columns = str(value[0])
                else:
                    columns += "," + str(value[0])
            return columns, names, dtype

        # load the tabula with a little method, params are the start
        # and end line numbers, and the cols that are to be used
        def loader(x, y, cols_used):
            columns, names, dtype = get_type_cols(cols_used)
            return pd.read_excel(
                path_tabula,
                sheet_name=sheet_name,
                usecols=columns,
                skiprows=x - 1,
                nrows=y - x + 1,
                header=None,
                names=names,
                dtype=dtype,
            )

        # load it all and decide later which ones to use
        rows_start = 147
        rows_end = 326

        tabula = loader(rows_start, rows_end, columns)
        # fill nans from merged cells with the correct texts
        tabula = tabula.fillna(method="ffill", axis=0)

        # split EFH_A, MFH_A, ... MFH_L to A, B, ..., L
        a_to_l, sealing = [], []
        for row in tabula.itertuples():
            a_to_l.append(row.building_code.split("_")[-1])
            sealing.append(max(row.floor1, row.floor2))

        tabula["a_to_l"] = a_to_l
        tabula["sealing"] = sealing

        return tabula


class InputLoader:
    """Load input from tables and return it as dataframe or dictionary.

    sfh = single family houses
    mfh = many family houses
    th = therasses
    """

    def get_linear_interpolation(self, a, b, c, d, e, years, div):
        """Calculate linear interpolation between the years of rates.

        Parameters:
            - a: parameter for 2020
            - b: parameter for 2030
            - c: parameter for 2040
            - d: parameter for 2050
            - e: parameter for 2060
            - years: list, [2020, 2030, 2040, 2050, 2060]
            - div: divergence from given parameters
        Returns:
            - linear: list of interpolated floats
        """
        # check whether divergence is value or list
        if isinstance(div, str):
            div_factor = div.split(",")
            div_factor = [float(i.strip()) for i in div_factor]
            div_str = True
        else:
            div_factor = [div]
            div_str = False
        div_factor = [1 - i for i in div_factor]

        def div_years(x, y, div_fact):
            # calculates ammount of divergence between two values x and y
            # y if divergence not set (div_fact=1)
            return (y - x) * div_fact + x

        def get_linear(x, y, years_diff):
            # y_new = div_years(x, y)
            linear_factor = (y - x) / years_diff
            linear = [x]
            for _ in range(years_diff - 1):
                linear.append(linear[-1] + linear_factor)
            return linear

        b = div_years(a, b, div_factor[0])
        if div_str is False:
            c = div_years(a, c, div_factor[0])
            d = div_years(a, d, div_factor[0])
            e = div_years(a, e, div_factor[0])
        else:
            c = div_years(a, c, div_factor[1])
            d = div_years(a, d, div_factor[2])
            e = div_years(a, e, div_factor[3])
        linear = (
            get_linear(a, b, years[1] - years[0])
            + get_linear(b, c, years[2] - years[1])
            + get_linear(c, d, years[3] - years[2])
            + get_linear(d, e, years[4] - years[3])
        )
        linear.append(e)
        return linear

    # @TODO: implement exponential interpolation
    def get_exponential_interpolation(self, a, b, c, d, e, years, div):
        """Calculate exponential interpolation between the years of rates.

        Not yet realized.

        Parameters:
            - a: parameter for 2020
            - b: parameter for 2030
            - c: parameter for 2040
            - d: parameter for 2050
            - e: parameter for 2060
            - years: list, [2020, 2030, 2040, 2050, 2060]
            - div: divergence from given parameters
        Returns:
            - exponential: list of exponentially interpolated floats
        """
        pass

    def load_param(self, path_param, key_years):
        """Load parameters and return yearly interpolated values.

        Parameters:
            - path_param: string, path to model-scenarios
            - key_years: list, [2020, 2030, 2040, 2050, 2060]

        Returns:
            - scen_params: dict, interpolated scenario parameters loaded from path_param
            - years: dict, {string: [], string: []}, years from 2019-2060 and 2020-2060
        """
        df = pd.read_excel(path_param, sheet_name=None, index_col=0)

        scen_params = {}
        for sc in df:
            scen_params[sc] = {}
            idx = df[sc].index.tolist()
            for i in range(len(df[sc])):
                line = df[sc].iloc[i]
                index = idx[i]
                # linear interpolation
                if line["interpolation"] == "linear":

                    def fun(a, b, c, d, e, years, div):
                        return self.get_linear_interpolation(a, b, c, d, e, years, div)

                # exponential interpolation
                elif line["interpolation"] == "exponential":

                    def fun(a, b, c, d, e, years, div):
                        return self.get_exponential_interpolation(
                            a, b, c, d, e, years, div
                        )

                # add params to dict of dicts
                scen_params[sc][index] = fun(
                    line[str(key_years[0])],
                    line[str(key_years[1])],
                    line[str(key_years[2])],
                    line[str(key_years[3])],
                    line[str(key_years[4])],
                    key_years,
                    line["divergence"],
                )
                years = {}
                years["years_19-60"] = [k for k in range(2019, 2061)]
                years["years_20-60"] = [k for k in range(2020, 2061)]
        return scen_params, years

    def load_hyperparameter(self, path_hyperparam):
        """Load hyperparemters from file."""
        df = pd.read_excel(path_hyperparam)
        hyperparameter = {}
        for i in range(len(df)):
            line = df.iloc[i]
            key = line["parameter"]
            value = line["value"]
            if key == "scenario":
                scen = value.split(",")
                hyperparameter[key] = [el.strip().lower() for el in scen]
            elif key == "restauration_building_type bias":
                value = value.lower()
                hyperparameter[key] = value
            else:
                hyperparameter[key] = value
        return hyperparameter


class RateCalculator:
    """Calculate the new building rate and the demolition rate."""

    def rates(self, total_living_space_2019, bev, scen_params, key_years):
        """Calculate ls and demolition and nb rates and return scenario parameters.

        Parameters:
            - total_livin_space_2019: float, total living space of 2019,
            - bev: population prediction variant,
            - scen_params: dict, interpolated scenario parameters
            - key_years: list, [2020, 2030, 2040, 2050, 2060]

        Returns:
            - scen_params: dict, updated with the following entries:
                - demolition_rate
                - new_building_rate
                - total_living_space
        """
        dr_min = scen_params["demolition_rate_min"]
        nbr_min = scen_params["new_building_rate_min"]
        ls_pc = scen_params["living_space_pc"]

        # calculate total living space for the whole timespan
        # based on population analysis and on prognosed living space
        ls_total = {"2019": total_living_space_2019}
        for i, (pop, ls) in enumerate(zip(bev, ls_pc)):
            ls_total[f"{2020+i}"] = pop * ls

        # calc living space from min (new_building_rate and demolition_rate)
        ls_calc = {"2019": total_living_space_2019}
        for i, (nbr, dr) in enumerate(zip(nbr_min, dr_min)):
            # calculated living space from next year is total living space
            # from this (i) year times the factor of Difference between new
            # builing rate and demolition rate of this year
            factor = 1.0 + nbr - dr
            ls_calc[f"{2020+i}"] = ls_total[f"{2019+i}"] * factor

        # compare total and calc_living_space
        new_building_rate = []
        demolition_rate = []
        for i in range(len(ls_total)):
            # case i=0: 2019 - our starting point - we don't provide 2018 data
            if i > 0:
                total = ls_total[f"{2019+i}"]
                total_prev = ls_total[f"{2019+i-1}"]
                calc = ls_calc[f"{2019+i}"]
                # print(f'total: {total} calc: {calc}')

                diff_rate = (total - calc) / total_prev
                # print(diff_rate)
                if diff_rate > 0:
                    # case too few buildings
                    new_building_rate.append(nbr_min[i - 1] + diff_rate)
                    demolition_rate.append(dr_min[i - 1])
                elif diff_rate < 0:
                    # case too many buildings
                    demolition_rate.append(dr_min[i - 1] - diff_rate)
                    new_building_rate.append(nbr_min[i - 1])
                else:
                    # case unrealistic
                    demolition_rate.append(dr_min[i - 1])
                    new_building_rate.append(nbr_min[i - 1])

        # for i in range(1, 41):
        #    print(ls_total[f'{2020+i}'] - ls_total[f'{2020+i-1}'])

        # TODO: in Hyperparameter, repair and add polynoms for hyperparams

        def get_smoothed_curve(rate):
            def get_gradient(x1, y1, x2, y2):
                m = (y2 - y1) / (x2 - x1)
                return m

            r = []
            offset = 0
            for yea in range(len(key_years) - 1):
                r_tmp = []
                off = key_years[yea + 1] - key_years[yea] + 1
                for yy in range(off):
                    r_tmp.append(rate[yy + offset])
                m = get_gradient(offset, r_tmp[0], offset + off, r_tmp[-1])

                for x, y in enumerate(r_tmp):
                    r.append(m * x + r_tmp[0])
                r.pop(0)
                offset += off - 1
            r.append(rate[-1])
            return r

        scen_params["demolition_rate"] = demolition_rate
        scen_params["new_building_rate"] = new_building_rate
        scen_params["total_living_space"] = ls_total

        return scen_params
