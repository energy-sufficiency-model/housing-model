"""Miscellaneous - globals, decorator defintions and parameters-class.

@author: guuug
"""

import os
import time
import scripts.inputs as inputs
from functools import wraps  # for letting decorated functions keep their docstring
import argparse  # for accepting command line arguments

# GLOBALS

# Distribution of demolition and restoration rates
# depending on the year of construction
# TODO: genauer raussuchen: own estimations based on bbsr
DISTRIBUTION_BUILDINGS = os.path.join("data", "distribution_buildings.xlsx")

# demographic developement of germany based on different
# population models
# source: https://www-genesis.destatis.de \
# /genesis//online?operation=table&code=12421-0001& \
# bypass=true&levelindex=0&levelid=1643115777925#abreadcrumb
DEMOGRAPHIC_DEVELOPEMENT = os.path.join("data", "12421-0001.xlsx")

# Building typology data from IWU (Institut Wohnen und Umwelt),
# https://www.iwu.de/fileadmin/tools/tabula/TABULA-Analyses_DE-Typology_DataTables.zip
TABULA = os.path.join("data", "TABULA-Analyses_DE-Typology_ResultData.xlsx")

# Weighting of the building typology in 2019 based on a weighted building
# typology from 2011 [1] and supplemented by energetic modernisation [2] and
# new buildings [3] and demolition [3, 4]
# [1] https://www.iwu.de/fileadmin/user_upload/dateien/energie/klima_altbau/ \
# Fl%C3%A4chen_Geb%C3%A4udetypologie_Okt_2013.pdf
# [2] https://www.iwu.de/fileadmin/publikationen/gebaeudebestand/ \
# 2018_IWU_CischinskyEtDiefenbach_Datenerhebung-Wohngeb%C3%A4udebestand-2016.pdf
# [3] TODO: genauer raussuchen: www.destatis.de
# [4] TODO: genauer raussuchen bbsr
SHARE_BUILDINGS_2019 = os.path.join("data", "share_buildings_2019.xlsx")

# Input data - different scenarios
# SCENARIOS = os.path.join('input', 'parameter_scenarios.xlsx')
SCENARIOS = os.path.join("input", "scenarios.xlsx")

# Hyperparameter
HYPERPARAMETER = os.path.join("input", "hyperparameter.xlsx")

METHODS_TIME = {}


# METHODS
def execution_time(func):
    """Count method execution time. Decorator definition."""

    @wraps(func)
    def inner(*args):
        a = time.time()
        args = func(*args)
        needed_time = time.time() - a

        # print(f'{needed_time} seconds for calculating {func.__name__}')

        def insert_k_v(k, v):
            if k in METHODS_TIME:
                METHODS_TIME[k] += v
            else:
                METHODS_TIME[k] = v

        if "plot" in func.__name__:
            insert_k_v("plots", needed_time)
        else:
            insert_k_v(func.__name__, needed_time)
        return args

    return inner


def add_to_dict(a, b):
    """Add contents of b to a, both are dicts."""
    for k, v in b.items():
        if k in a:
            a[k] += v
        else:
            a[k] = v
    return a


def print_debug(msg):
    """Mark debug prints when debug mode is run."""
    print(f"DEBUG_INFO: {msg}")


# CLASSES
class Parameter:
    """Parameter class. Access Hyperparameter and runtime parameter. Load data."""

    def __init__(self, debug_flag, dis_mon_prot_flag):
        """Initialize Parameter class.

        Parameters:
            - debug_flag: if set, outputs runtime information on console and saves more
              information to csv during run.
            - dis_mon_prot_flag: if set, monument protection will be disabled during run
        """
        self.debug_flag = debug_flag
        self.dis_mon_prot_flag = dis_mon_prot_flag

        # TODO: add as parameter for running code. Change globally to make
        # the code also run with different years
        self.key_years = [2020, 2030, 2040, 2050, 2060]

        # load inputs
        il = inputs.InputLoader()
        self.hyperparameter = il.load_hyperparameter(HYPERPARAMETER)
        scen_params_raw, self.years = il.load_param(SCENARIOS, self.key_years)

        # load data
        dl = inputs.DataLoader()
        self.dem_dev = dl.load_demographic_developement(DEMOGRAPHIC_DEVELOPEMENT)

        self.d_dist = [float(x) for x in self.hyperparameter["demolition"].split(", ")]
        self.dist_buildings = dl.load_dist_buildings(DISTRIBUTION_BUILDINGS)
        self.chosen_scenarios = self._get_chosen_scens(scen_params_raw)
        # output_folder = self.hyperparameter['output_folder']
        self.bev_all = self.dem_dev[self.hyperparameter["bev_variant"]]
        self.bev = self.dem_dev[self.hyperparameter["bev_variant"]][1:]
        self.scen_params = self._get_scen_params(dl, scen_params_raw)
        self.results_name = "results.xlsx"

        # define more globals
        self.bv_001 = "001"  # building_variant_001
        self.bv_002 = "002"
        self.bv_003 = "003"

        self._check_hyperparameter(scen_params_raw)

    def _check_hyperparameter(self, scen_params_raw):
        """Check all loaded hyperparameters for correct format and behaviour."""

        def is_in_check(checklist, check_against, param):
            if checklist not in check_against:
                raise Exception(
                    f"wrong {param} given in input/hyperparameter.xlsx:\
                            {checklist}"
                )

        bev_variants = [
            "BEV-VARIANTE-{}".format(str(i + 1).zfill(2)) for i in range(21)
        ]
        bev_models = ["BEV-MODELL-{}".format(str(i + 1).zfill(2)) for i in range(9)]
        bev = bev_variants + bev_models

        scens = list(scen_params_raw.keys())
        scens.append("all")
        yes_no = ["yes", "no"]

        hyper_checkdict = {
            "bev_variant": bev,
            "scenario": scens,
            "restauration_building_type bias": yes_no,
            "second_amb_restauration": yes_no,
            "demolish_restored_buildings": yes_no,
        }

        # see for hyperparams in hyper_checklist whether they appear in
        # the given lists (the values in the dict)
        for param, check_against in hyper_checkdict.items():
            if isinstance(self.hyperparameter[param], list):
                for pa in self.hyperparameter[param]:
                    is_in_check(pa, check_against, param)
            else:
                is_in_check(self.hyperparameter[param], check_against, param)

        # ensure the output folder to be of type string
        if not isinstance(self.hyperparameter["output_folder"], str):
            raise Exception(
                "output folder in wrong format in input/hyperparameter.xlsx"
            )

        # ensure that monument protection is between 0...1
        if (
            float(self.hyperparameter["monument_protection"]) < 0
            or float(self.hyperparameter["monument_protection"]) > 1
        ):
            raise Exception(
                "monument_protection must be between 0...1 in \
                    input/hyperparameter.xlsx"
            )

        # ensure that demolition for the building variant is not greater or
        # less than 1
        if sum(self.d_dist) != 1.0:
            raise Exception("demolition must sum up to 1 in input/hyperparameter.xlsx")

    def _get_chosen_scens(self, scen_params_raw):
        """Either load all scenarios or the hyperparameter given one."""
        if "all" in self.hyperparameter["scenario"]:
            scens = list(scen_params_raw.keys())
            return sorted(scens)
        else:
            return self.hyperparameter["scenario"]

    def _get_scen_params(self, dl, scen_params_raw):
        """Load rate calculator and return yearly scenario parameters."""
        _, total_living_space_2019 = dl.load_share_buildings(SHARE_BUILDINGS_2019)

        # calculate rates
        rc = inputs.RateCalculator()
        scen_params = {}
        for scen in self.chosen_scenarios:
            params = rc.rates(
                total_living_space_2019, self.bev, scen_params_raw[scen], self.key_years
            )
            scen_params[scen] = params
        return scen_params

    def get_tabula(self):
        """Load and initialize tabula dataframe.

        Returns:
            - tabula_buildings: central dataframe for living space calculations.
            - tabula_heat: central dataframe for space heat need calculations.
            - tabula_sealing: central dataframe for sealing calculations.
        """
        dl = inputs.DataLoader()
        df_share_buildings, _ = dl.load_share_buildings(SHARE_BUILDINGS_2019)
        # exclude 2019
        df_share_buildings = df_share_buildings.loc[
            df_share_buildings["living_space_2019"] > 0
        ]

        tabula = dl.load_tabula(TABULA)
        tabula = tabula.merge(
            df_share_buildings, left_on="identifier", right_on="tabula_code"
        )

        if not self.dis_mon_prot_flag:
            # monument protection for different building classes limited by
            # hyperparameter (e.g. 5% of buildings) and by building
            # distribution (e.g. 30% of building type A, 30% of building
            # type B, ...)
            tls_2019 = self.scen_params[self.chosen_scenarios[0]]["total_living_space"][
                "2019"
            ]
            monument_protection = (
                self.dist_buildings["monument protection"]
                * tls_2019
                * float(self.hyperparameter["monument_protection"])
            )

            # add monument protection to tabula
            tabula = tabula.merge(
                monument_protection, left_on="a_to_l", right_index=True
            )

        # calculate the sum of living space of each building age class
        # (A, ..., L) and attach it to dist_r_d
        dist_grouped = tabula.groupby(by="a_to_l").sum(numeric_only=True)
        tabula = tabula.merge(dist_grouped["living_space_2019"], on="a_to_l")
        drops = [
            "tabula_code",
            "living_space_2019_x",
            "living_space_2019_y",
            "floor1",
            "floor2",
        ]
        tabula["living_space_2019"] = tabula["living_space_2019_x"]
        building_cols = [
            "identifier",
            "building_code",
            "building_type",
            "a_to_l",
            "building_variant",
            "energy_reference_area",
            "percent_living_space",
            "living_space_2019",
        ]
        heat_cols = [
            "identifier",
            "energy_reference_area",
            "space_heat_need_m2",
            "hot_water_need_m2",
            "heat_provided",
            "hot_water_provided",
        ]
        sealing_cols = ["identifier", "energy_reference_area", "sealing"]

        if not self.dis_mon_prot_flag:
            # calculate the share of the living space from each building type
            # in according building age class (e.g. EFH_A has a share of 0.74%
            # of all buildings in A)
            monument_map = tabula["building_variant"] == self.bv_001

            tabula["monument_protection"] = (
                tabula["living_space_2019_x"].loc[monument_map]
                / tabula["living_space_2019_y"].loc[monument_map]
            ) * tabula["monument protection"].loc[monument_map]

            drops.append("monument protection")
            building_cols.append("monument_protection")

        tabula.drop(drops, axis=1, inplace=True)
        tabula_buildings = tabula.loc[:, building_cols]
        tabula_heat = tabula.loc[:, heat_cols]
        tabula_sealing = tabula.loc[:, sealing_cols]

        return tabula_buildings, tabula_heat, tabula_sealing

def parse_arguments():
    # first parse optional command line arguments
    parser = argparse.ArgumentParser(
        description="Arguments are optional for debugging purposes."
    )
    parser.add_argument(
        "-d,",
        "--debug",
        action="store_true",
        default=False,
        help="debug better with more information.",
    )

    parser.add_argument(
        "-m,",
        "--dis_mon_prot",
        action="store_true",
        default=False,
        help="disable monument protection from model.",
    )
    args = parser.parse_args()
    debug_flag = args.debug
    dis_mon_prot_flag = args.dis_mon_prot
    return debug_flag, dis_mon_prot_flag