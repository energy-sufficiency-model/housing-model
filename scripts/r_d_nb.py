"""Calculate Restoration (r), Demolition (d) and New Buildings (nb).

@author: guuug
"""

import pandas as pd
from scripts.misc import execution_time


def calc_r_d_ic(dist_r_d, params, r_d_area_i, dist_buildings_r_d, r_d_ls, r_d):
    """Calculate percentage - distribution of house-types.

    In the different house classes (e.g. A: MFH-50%, EFH-20%, ...).

    Parameters:
        - dist_r_d: dataframe, used for calculating distributions of restoration
          and demolition. Central for calculations of restoration and demolition.,
        - params: parameter class to access settings for the model.,
        - r_d_area_i: area of current year to be restorated or demolished.,
        - dist_buildings_r_d: string, "demolition" or "restoration,
        - r_d_ls: string,
            if restoration: living space from one year earlier / df column
            if demolition: calc living space from current year / df column
        - r_d: string, either "r" (restoration), or "d" (demolition).,

    Returns:
        - dist_r_d: update dist_r_d with:
            - merged with sum of dist_r_d grouped by a_to_l,
            - share_buildings column,
            - if no restoration building type bias set:
                - merged with param given distribution of r/d in building age classes,
                - r_d_ic column.
        - r_d_ls: String, living space column name.
    """
    # calculate the sum of living space for each building age class (A, ..., L)
    # and attach it to dist_r_d
    dist_grouped = dist_r_d.groupby(by="a_to_l").sum(numeric_only=True)
    dist_r_d = dist_r_d.merge(dist_grouped[r_d_ls], on="a_to_l")

    # calculate the share of the living space from each building type in the
    # according building age class (e.g. EFH_A has a share of 0.74% of all
    # buildings in A)
    dist_r_d["share_buildings"] = dist_r_d[f"{r_d_ls}_x"] / dist_r_d[f"{r_d_ls}_y"]

    # parameter-given distribution of restoration/demolition in the
    # building age classes (e.g. A gets 15% of available restoration potential)
    db_r_d = params.dist_buildings[dist_buildings_r_d]

    if params.hyperparameter["restauration_building_type bias"] == "no":
        dist_r_d = dist_r_d.merge(db_r_d, left_on="a_to_l", right_index=True)
        dist_r_d[f"{r_d}_ic"] = (
            dist_r_d["share_buildings"] * r_d_area_i * dist_r_d[dist_buildings_r_d]
        )

    elif params.hyperparameter["restauration_building_type bias"] == "yes":
        print("NOT YET")
    r_d_ls = f"{r_d_ls}_x"
    return dist_r_d, r_d_ls


def remaining_r_d(dist_r_d, r_d_ls, r_d):
    """Calculate r_d_final, r_d_rem_b and r_d_rem.

    r_d_final:
        If the intermediate calculation of restoration/demolition space
        fits into the assigned space for restoration/demolition (r_d_ls)
        (can't restorate/demolate more space than the available), it is
        accepted where it is assigned to (e.g. to EFH_A).

        Else as much as possible (all available space) is used for
        restoration/demolition, the rest is to be distributed at
        building_variant 002.

    r_d_rem_b:
        mark binary whether the intermediate calculation of r/d space
        (r_d_ic) fits into living space or not.
        To see whether space remains or not.
        1: if r_d_ic fits into assigned space for r/d marks that restoration area can
        be assigned
        0: if r_d_ic fits not into assigned space for r/d marks that no more
        restoration area can be assigned

    r_d_rem:
        finds the remaining restoration/demolition space that needs
        to be further distributed

    Parameters:
        - dist_r_d: dataframe, used for calculating distributions of restoration and
          demolition. Central for calculations of restoration and demolition.
            - Updated with r_d_final, r_d_rem_b and r_d_rem
        - r_d_ls: string, if restoration: living space from one year earlier / df column
          if demolition: calc living space from current year / df column
        - dis_mon_prot_flag: disable monument protection - standard: False
          if set, monument protection will be disabled

    Returns:
        - dist_r_d that contains the new _final, _rem_b, _rem columns
    """
    r_d_final, r_d_rem_b, r_d_rem = [], [], []
    for row in dist_r_d.itertuples():
        r_d_ic = getattr(row, f"{r_d}_ic")
        if r_d_ic <= row.free_ls:
            r_d_final.append(r_d_ic)
            r_d_rem.append(0)

            if r_d_ic < row.free_ls:
                if r_d_ic == 0:
                    r_d_rem_b.append(0)
                else:
                    r_d_rem_b.append(1)
            else:
                r_d_rem_b.append(0)
        else:
            r_d_final.append(row.free_ls)
            r_d_rem_b.append(0)
            r_d_rem.append(r_d_ic - row.free_ls)

    dist_r_d[f"{r_d}_final"] = r_d_final
    dist_r_d[f"{r_d}_rem_b"] = r_d_rem_b
    dist_r_d[f"{r_d}_rem"] = r_d_rem

    return dist_r_d


def merge_r_d_final(dist_r_d, tabula_buildings, r_d_col, bv_r_d, params, r_d):
    """Create r_d_final dataframe from dist_r_d and merge r_d_final into
    tabula_buildings.

    Parameters:
        - dist_r_d: dataframe, used for calculating distributions of restoration and
          demolition. Central for calculations of restoration and demolition.,
        - tabula_buildings: dataframe, central df in the whole model.,
        - r_d_col: restoration/demolition - area / df column.,
        - bv_r_d: building variant or restoration or demolition.,
        - params: parameter class to access settings for the model.,
        - r_d: string, either "r" (restoration), or "d" (demolition).,

    Returns:
        - tabula_buildings: dataframe, central df in the whole model. Updated with:
            - merged with r_d_final (bv_r_d == 001), or
            - updated r_d_col from r_d_final (bv_r_d == 002)
    """
    r_d_id = f"{r_d}_id"
    r_d_final = dist_r_d[[f"{r_d}_final", "identifier"]].copy()
    r_d_final.rename(
        columns={f"{r_d}_final": r_d_col, "identifier": r_d_id}, inplace=True
    )
    if bv_r_d == params.bv_001:
        tabula_buildings = tabula_buildings.merge(
            right=r_d_final, left_on="identifier", right_on=r_d_id, how="left"
        )
        tabula_buildings.drop([r_d_id], axis=1, inplace=True)

    else:  # bv_r_d == params.bv_002:
        for row in r_d_final.itertuples():
            tabula_buildings.loc[
                tabula_buildings["identifier"] == getattr(row, r_d_id), r_d_col
            ] = getattr(row, r_d_col)
    return tabula_buildings


def calc_r_d_final(
    tabula_buildings, params, r_d_area_i, current_year, r_d_ls, r_d_col, bv_r_d, is_r
):
    """Calc r or d area for each building age class, dependand on r_d_area_i.

    Parameters:
        - tabula_buildings: dataframe, central df in the whole model.,
        - params: parameter class to access settings for the model.,
        - r_d_area_i: area of current year to be restorated or demolished.,
        - current year: int, name of year the model currently calculates (2020-2060),
        - r_d_ls: string, if restoration: living space from one year earlier / df column
          if demolition: calc living space from current year / df column
        - r_d_col: restoration/demolition - area / df column
        - bv_r_d: building variant or restoration or demolition
        - is_r: boolean, True for restoration, False for demoliton
    Returns:
        - tabula_buildings: dataframe, central df in the whole model
        - r_d_carryover_002: carryover from r/d area that can't be distributed
          in bv_r_d==001 and needs to be distribued in bv_r_d==002
        - dists: dict of dataframes, r/d areas for each building age class
          and names for saving the dataframes to disk
    """
    r_d_carryover_002 = 0  # sum of restauration that can't be distributed

    # set parameter to distinguish demolition from restoration in building
    # age class distribution
    dist_buildings_r_d = "demolition"
    r_d = "d"
    if is_r:
        dist_buildings_r_d = "restoration"
        r_d = "r"

    save_name = f"dist_{r_d}_{current_year}_{dist_buildings_r_d}_{bv_r_d}.xlsx"

    dist_cols = ["identifier", "building_code", "a_to_l", r_d_ls]
    if not params.dis_mon_prot_flag and bv_r_d == params.bv_001:
        dist_cols.append("monument_protection")

    # dataframe used for all calculations here. Distributions.
    dist_r_d = tabula_buildings.loc[
        tabula_buildings["building_variant"] == bv_r_d, dist_cols
    ].copy()

    # (intermediate) calculation of possible final restoration/demolition area
    # per building type _ building age class
    # (e.g. EFH_A gets 8.40952 mio m2 restoration area)
    dist_r_d, r_d_ls = calc_r_d_ic(
        dist_r_d, params, r_d_area_i, dist_buildings_r_d, r_d_ls, r_d
    )

    if params.dis_mon_prot_flag or bv_r_d != params.bv_001:
        dist_r_d["free_ls"] = dist_r_d[r_d_ls]
    else:
        dist_r_d["free_ls"] = dist_r_d[r_d_ls] - dist_r_d["monument_protection"]

    # find out whether the intermediate calculation of r/d area can be
    # distributed into the available living space.
    dist_r_d = remaining_r_d(dist_r_d, r_d_ls, r_d)

    dist_r_d_save = {}

    if params.debug_flag:
        while_index = 0
        dist_r_d_save["before loop"] = dist_r_d.copy()

    while True:
        # check for area to be restaurated/demolished.
        # No area left, nothing to do
        r_d_sum_ic = dist_r_d.loc[dist_r_d[f"{r_d}_rem_b"] == 1, f"{r_d}_final"].sum()

        # calculate the share of r/d area to be distributed. Some of this area
        # has already been distributed, marked by the r_d_rem_b column
        dist_r_d[f"{r_d}_share"] = (
            dist_r_d[f"{r_d}_final"] / r_d_sum_ic * dist_r_d[f"{r_d}_rem_b"]
        )

        # total remaining r/d area of buidlings
        r_d_total_rem = r_d_area_i - dist_r_d[f"{r_d}_final"].sum()
        # print(r_d_total_rem - sum(dist_r_d['r_d_rem']))

        r_d_total_rem = r_d_area_i - r_d_sum_ic

        if (
            len(dist_r_d.loc[dist_r_d[f"{r_d}_rem_b"] == 1]) == 0
            or r_d_total_rem < 0.000001
        ):
            r_d_carryover_002 = dist_r_d[f"{r_d}_rem"].sum()
            break

        # the area that is going to be added to the restoration/demolition area
        # the sum of add_r_d col equals the r_d_total_rem
        dist_r_d[f"add_{r_d}"] = dist_r_d[f"{r_d}_share"] * r_d_total_rem

        dist_r_d[f"{r_d}_ic"] = dist_r_d[f"{r_d}_final"] + dist_r_d[f"add_{r_d}"]

        dist_r_d = remaining_r_d(dist_r_d, r_d_ls, r_d)

        r_d_total_rem = r_d_area_i - dist_r_d[f"{r_d}_final"].sum()

        if params.debug_flag:
            while_index += 1
            dist_r_d_save[f"loop_{while_index}"] = dist_r_d.copy()

    tabula_buildings = merge_r_d_final(
        dist_r_d, tabula_buildings, r_d_col, bv_r_d, params, r_d
    )
    dists = {
        bv_r_d: {
            f"dist_{r_d}_{bv_r_d}": dist_r_d_save,
            f"dist_save_name_{bv_r_d}": save_name,
        }
    }
    return tabula_buildings, r_d_carryover_002, dists


def apply_r_d(
    tabula_buildings,
    r_d_col,
    r_deep_amb,
    current_year,
    old_calc_ls,
    calc_ls,
    params,
    is_r,
):
    """Apply new calc_ls to ls in tabula_buildings.

    Iterate through tabula_buildings. Starting with building variants 001 up to 003.
    During iteration, append old_calc_ls - r_d_col for bv_001 to ls and fill a
    ls_adder dict to be used for bv_002 and bv_003 (only restoration).

    Parameters:
        - tabula_buildings: central dataframe for living space calculations.,
        - r_d_col: restoration/demolition - area / df column,
        - r_deep_amb: list, restoration deep ambitious input parameter for current year,
        - current_year: int, name of year the model currently calculates (2020-2060),
        - old_calc_ls: string, old_ls for restoration, calc_ls for demolition,
        - calc_ls: string, column-name for calculated living space in current year,
        - params: parameter class to access settings for the model,
        - is_r: boolean, True for restoration, False for demoliton

    Returns:
        - tabula_buildings: updated with calc_ls column.
    """
    ls_adder = {}
    ls = []
    for row in tabula_buildings.itertuples():
        if not is_r or row.building_variant == params.bv_001:
            ls.append(getattr(row, old_calc_ls) - getattr(row, r_d_col))
            if is_r:
                ls_adder[f"{row.building_code}_2"] = (1 - r_deep_amb) * getattr(
                    row, r_d_col
                )
                ls_adder[f"{row.building_code}_3"] = r_deep_amb * getattr(row, r_d_col)

        elif is_r and row.building_variant == params.bv_002:
            ls.append(ls_adder[f"{row.building_code}_2"] + getattr(row, old_calc_ls))

        elif is_r and row.building_variant == params.bv_003:
            ls.append(ls_adder[f"{row.building_code}_3"] + getattr(row, old_calc_ls))

    tabula_buildings[calc_ls] = ls
    return tabula_buildings


@execution_time
def restauration(
    r_area_i, r_deep_amb, params, current_year, tabula_buildings, old_ls, calc_ls, r_col
):
    """Apply the restoration to the current year's living space.

    Parameters:
        - r_area_i: float, area of current year to be restored,
        - r_deep_amb: list, restoration deep ambitious input parameter for current year,
        - params: parameter class to access settings for the model,
        - current_year: int, name of year the model currently calculates (2020-2060),
        - tabula_buildings: central df for living space calculations.,
        - old_ls: string, column-name for living space of year before current year,
        - calc_ls: string, column-name for calculated living space in current year,
        - r_col: string, column-name for restoration area in current year,

    Returns:
        - tabula_buildings: central df for living space calculations. Updated by:
            - def calc_r_d_final
            - def apply_r_d,
        - dists_001: dict of dataframes, restoration areas for each building age class
          and names for saving the dataframes to disk
    """
    # restoration calculation begins
    tabula_buildings, r_carryover_002, dists_001 = calc_r_d_final(
        tabula_buildings,
        params,
        r_area_i,
        current_year,
        old_ls,
        r_col,
        params.bv_001,
        is_r=True,
    )
    if (
        params.hyperparameter["second_amb_restauration"] == "yes"
        and r_carryover_002 > 0
    ):
        tabula_buildings, _, dists_002 = calc_r_d_final(
            tabula_buildings,
            params,
            r_carryover_002,
            current_year,
            old_ls,
            r_col,
            params.bv_002,
            is_r=True,
        )
        dists_001 = {**dists_001, **dists_002}  # merging

    tabula_buildings = apply_r_d(
        tabula_buildings,
        r_col,
        r_deep_amb,
        current_year,
        old_ls,
        calc_ls,
        params,
        is_r=True,
    )
    return tabula_buildings, dists_001


@execution_time
def demolition(d_area_i, params, current_year, tabula_buildings, calc_ls, d_col):
    """Apply the demolition to the current year's living space.

    Parameters:
        - d_area_i: float, area of current year to be demolished,
        - params: parameter class to access settings for the model,
        - current_year: int, name of year the model currently calculates (2020-2060),
        - tabula_buildings: central dataframe for living space calculations.,
        - calc_ls: string, column-name for calculated living space in current year,
        - d_col: string, column-name for demolition area in current year,

    Returns:
        - tabula_buildings: central df for living space calculations. Updated by:
            - def calc_r_d_final
            - def apply_r_d
        - dists: dict of dataframes, demolition areas for each building age class
          and names for saving the dataframes to disk
    """
    # total ls from last year (initially 2019)
    dists = {}

    carryover_area = 0
    for area, bv in zip(
        [d_area_i * d for d in params.d_dist],
        [params.bv_001, params.bv_002, params.bv_003],
    ):
        tabula_buildings, d_carryover_00x, dists_00x = calc_r_d_final(
            tabula_buildings,
            params,
            area + carryover_area,
            current_year,
            calc_ls,
            d_col,
            bv,
            is_r=False,
        )

        if (
            params.hyperparameter["demolish_restored_buildings"] == "yes"
            and d_carryover_00x > 0
        ):
            # give carryover area to area in next round
            carryover_area = d_carryover_00x
        dists = {**dists, **dists_00x}  # merging

    tabula_buildings = apply_r_d(
        tabula_buildings,
        d_col,
        None,
        current_year,
        calc_ls,
        calc_ls,
        params,
        is_r=False,
    )
    return tabula_buildings, dists


@execution_time
def new_buildings(
    nb_area_i, nb_amb, nb_spec_areas, tabula_buildings, calc_ls, new_ls, nb_col
):
    """Apply new buildings to the current year's living space.

    Parameters:
        - nb_area_i: float, area of current year reserved for new buildings,
        - nb_amb: list, new buildings deep ambitious input parameter for current year,
        - nb_spec_areas, dict, special areas (sfh, th, mfh) with sepcial new building
          areas,
        - tabula_buildings: dataframe, central df in the whole model,
        - calc_ls: string, column-name for calculated living space in current year,
        - new_ls: string, column-name for living space in current year,
        - nb_col: string, column-name for new building area in current year,

    Returns:
        - tabula_buildings: dataframe, central df in the whole model. Updated with:
            - nb_col
            - new_ls
    """
    df_col = pd.DataFrame({nb_col: [0 for _ in range(len(tabula_buildings))]})
    bcs = ["EFH_L", "MFH_L", "RH_L"]
    areas = [
        nb_spec_areas["nb_sfh_area"],
        nb_spec_areas["nb_mfh_area"],
        nb_spec_areas["nb_th_area"],
    ]
    for bc, area in zip(bcs, areas):
        bc_map = tabula_buildings["building_code"] == bc
        bv_002 = tabula_buildings["building_variant"] == "002"
        bv_003 = tabula_buildings["building_variant"] == "003"

        nb_L_map_002 = bc_map & bv_002
        df_col[nb_col].loc[nb_L_map_002] = area * (1 - nb_amb)

        nb_L_map_003 = bc_map & bv_003
        df_col[nb_col].loc[nb_L_map_003] = area * nb_amb
    tabula_buildings.loc[:, nb_col] = df_col
    tabula_buildings.loc[:, new_ls] = (
        tabula_buildings[calc_ls] + tabula_buildings[nb_col]
    )
    return tabula_buildings
